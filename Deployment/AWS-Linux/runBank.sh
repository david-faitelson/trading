export CLASSPATH=$HOME/lib/Auth.jar:$HOME/lib/AuthAPI.jar:$HOME/lib/Bank.jar:$HOME/lib/BankAPI.jar:$HOME/db-derby-10.13.1.1-bin/lib/derby.jar

cd $HOME/trading

PUBLIC_IP=$(sh $HOME/bin/pubip.sh)

nohup java -Djava.rmi.server.hostname=$PUBLIC_IP bank.server.BankServer 5183 &


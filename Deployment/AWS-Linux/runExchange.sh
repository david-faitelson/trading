export CLASSPATH=$HOME/lib/Auth.jar:$HOME/lib/AuthAPI.jar:$HOME/lib/BankAPI.jar:$HOME/lib/Exchange.jar:$HOME/lib/ExchangeAPI.jar:$HOME/db-derby-10.13.1.1-bin/lib/derby.jar

cd $HOME/trading

PUBLIC_IP=$(sh $HOME/bin/pubip.sh)

nohup java -Djava.rmi.server.hostname=$PUBLIC_IP exchange.server.ExchangeServer 5831 &


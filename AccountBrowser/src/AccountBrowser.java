
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.GregorianCalendar;
import java.util.Scanner;

import bank.api.BankManager;
import bank.api.DoesNotHaveThisAssetException;
import bank.api.InternalServerErrorException;
import bank.api.NotEnoughAssetException;
import bank.api.Transaction;

import auth.api.WrongSecretException;

public class AccountBrowser {

	public static void main(String[] args) throws MalformedURLException, RemoteException, NotBoundException, InternalServerErrorException {
		
		if (args.length != 1) {
			System.out.println("Usage: java AccountBrowser <hostname>");
			System.exit(1);
		}

		new AccountBrowser(args[0]).run();
	}

	private BankManager bank;
	private Scanner scanner;

	public AccountBrowser(String hostName) throws MalformedURLException, RemoteException, NotBoundException {
		bank = (BankManager) Naming.lookup("rmi://"+hostName+"/Bank");
		scanner = new Scanner(System.in);		
	}
	
	public void run() throws RemoteException, InternalServerErrorException {
		
		
		System.out.println("Please select an operation:\n1 - show assets \n2 - show transactions\n3 - transfer assets\n4 - exit");

		Integer opCode = scanner.nextInt();
		
		while(opCode != 4) {
			switch(opCode) {
			case 1:	showAllAssets(); break;
			case 2:	showAllTransactions(); break;
			case 3: transferAssets(); break;
			}
			System.out.println("Please select an operation:\n1 - show assets \n2 - show transactions\n3 - transfer assets\n4 - exit");
			opCode = scanner.nextInt();
		}
		
		scanner.close();
		System.out.println("Goodbye!");
	}

	private void transferAssets() throws RemoteException {
		// TODO Auto-generated method stub
		
		System.out.println("Please enter account identifier: ");
		Integer sourceAccountId = scanner.nextInt();
		System.out.println("Please enter account password: ");
		String password = scanner.next();

		System.out.println("Please enter destination account identifier: ");
		Integer destinationAccountId = scanner.nextInt();

		System.out.print("Please enter asset name: ");
		String assetName = scanner.next();
		
		System.out.print("Please enter amount to tranfer: ");
		Integer amount = scanner.nextInt();
		
		try {
			bank.transferAssets(password, sourceAccountId, destinationAccountId, assetName, amount);
		} catch (DoesNotHaveThisAssetException e) {
			System.out.println("The account " + e.getAccount() + " does not have the asset " + e.getAsset());
		} catch (NotEnoughAssetException e) {
			System.out.println("The account " + e.getAccount() + " has only " + e.getCurrentAmount() + " units  of " + e.getAsset());
		} catch (WrongSecretException e) {
			System.out.println("The password is wrong");
		} catch (InternalServerErrorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void showAllAssets() throws RemoteException, InternalServerErrorException {

		System.out.println("Please enter account identifier: ");
		Integer accountId = scanner.nextInt();
		System.out.println("Please enter account password: ");
		String password = scanner.next();

		try {
			for(String asset : bank.getAssets(password, accountId)) {
				Integer quantity = bank.getQuantityOfAsset(password,accountId, asset);
				System.out.println(quantity + " " + asset);
			}
		}catch(DoesNotHaveThisAssetException e) {
			System.out.println("The account " + e.getAccount() + " does not have the asset " + e.getAsset());
		}catch(WrongSecretException e) {
			System.out.println("The password is wrong");
		}
	}
	
	public void showAllTransactions() throws RemoteException, InternalServerErrorException {

		System.out.println("Please enter account identifier: ");
		Integer accountId = scanner.nextInt();
		System.out.println("Please enter account password: ");
		String password = scanner.next();

		try {
			for(Transaction tx : bank.getTransactionsSince(password, accountId, new GregorianCalendar(1900, 0, 0).getTime())) {
				System.out.println(tx.getExecutionDate() + " " + tx.getKind() + " " + tx.getAsset() + " " + tx.getAmount() + " from " + tx.getSource() + " to " + tx.getDestination());
			}
		}catch(WrongSecretException e) {
			System.out.println("The password is wrong");
		}
	
	}
}

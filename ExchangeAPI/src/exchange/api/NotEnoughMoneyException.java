package exchange.api;

public class NotEnoughMoneyException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3014323560052728998L;
	private Integer account;
	private Integer currentAmount;
	private Integer requestedAmount;

	public NotEnoughMoneyException(Integer account, Integer currentAmount, Integer requestedAmount) {
		this.account = account;
		this.currentAmount = currentAmount;
		this.requestedAmount = requestedAmount;
	}

	public Integer getAccount() {
		return account;
	}

	public void setAccount(Integer account) {
		this.account = account;
	}

	public Integer getCurrentAmount() {
		return currentAmount;
	}

	public void setCurrentAmount(Integer currentAmount) {
		this.currentAmount = currentAmount;
	}

	public Integer getRequestedAmount() {
		return requestedAmount;
	}

	public void setRequestedAmount(Integer requestedAmount) {
		this.requestedAmount = requestedAmount;
	}
}

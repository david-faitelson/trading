import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Map;

import bank.api.BankManager;
import exchange.api.ExchangeManager;

public class TradingClient {
	
	public static void main(String[] args) throws MalformedURLException, RemoteException, NotBoundException {
		
		BankManager bank = (BankManager) Naming.lookup("rmi://localhost/Bank");
		ExchangeManager exchange = (ExchangeManager) Naming.lookup("rmi://localhost/Exchange");
		
		for(String stockName : exchange.getStockNames()) {
		
			System.out.println("Supply of stock " + stockName);
			System.out.println("Price\tAmount");
			Map<Integer, Integer> supply = exchange.getSupply(stockName);
			for(Map.Entry<Integer, Integer> quote : supply.entrySet()) {
				System.out.println(quote.getKey() + "\t" + quote.getValue());
			}
	
			System.out.println("Demand for stock " + stockName);
			System.out.println("Price\tAmount");
			Map<Integer, Integer> demand = exchange.getDemand(stockName);
			for(Map.Entry<Integer, Integer> quote : demand.entrySet()) {
				System.out.println(quote.getKey() + "\t" + quote.getValue());
			}
		}
	}
}

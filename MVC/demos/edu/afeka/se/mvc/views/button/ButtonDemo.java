package edu.afeka.se.mvc.views.button;

import java.awt.Color;

import edu.afeka.se.geom.Point;
import edu.afeka.se.geom.Rectangle;
import edu.afeka.se.graphics.Display;
import edu.afeka.se.mvc.View;
import edu.afeka.se.mvc.views.button.ButtonController;
import edu.afeka.se.mvc.views.button.ButtonModel;
import edu.afeka.se.mvc.views.button.ButtonView;

public class ButtonDemo {

	 public static void main(String[] args) {
		 
		 try {
			 View view = new View().windowViewport(new Rectangle(new Point (0.0,  0.0), new Point(1.0,1.0)), new Rectangle(new Point(100.0,100.0), new Point(400.0, 400.0)));
			 view.setBackgroundColor(Color.gray);
			 
			 ButtonView buttonView = (ButtonView)new ButtonView().windowViewport(new Rectangle(new Point(0.0, 0.0), new Point(1.0, 1.0)), new Rectangle(new Point(0.0, 0.75), new Point(0.25,1.0)));
			 buttonView.setBackgroundColor(Color.darkGray);
			 buttonView.setButtonBorderSize(0.10);
			 buttonView.setPressedColor(Color.green);
			 buttonView.setUnpressedColor(Color.gray);
			 buttonView.setController(new ButtonController());
			
			 ButtonModel buttonModel = new ButtonModel();
			 buttonView.setModel(buttonModel);
			 buttonView.getController().setModel(buttonModel);
			 
			 view.addSubview(buttonView);

			 view.display();
			 
			 view.getController().startUp();
			 
			 try { Thread.sleep(1000); } catch(InterruptedException e) {} 

		} finally {
        	Display.instance().close();
        	System.exit(0);
        }
	 }
}

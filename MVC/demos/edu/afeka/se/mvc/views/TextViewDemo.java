package edu.afeka.se.mvc.views;

import java.awt.Color;

import edu.afeka.se.geom.Point;
import edu.afeka.se.geom.Rectangle;
import edu.afeka.se.graphics.Display;
import edu.afeka.se.mvc.View;
import edu.afeka.se.mvc.views.TextView;

public class TextViewDemo {


	 public static void main(String[] args) {
		 
		 try {
			 View view = new View().windowViewport(new Rectangle(new Point (0.0,  0.0), new Point(1.0,1.0)), new Rectangle(new Point(100.0,100.0), new Point(400.0, 400.0)));
			 view.setBackgroundColor(Color.gray);
			 
			 TextView textView = new TextView("Hello, world");
			 textView.windowViewport(new Rectangle(new Point(0.0, 0.0), new Point(1.0, 1.0)), new Rectangle(new Point(0.0, 0.0), new Point(1.0,0.125)));
			 textView.setBackgroundColor(Color.yellow);
					 
			 view.addSubview(textView);

			 view.display();
			 
			 view.getController().startUp();
			 
			 try { Thread.sleep(1000); } catch(InterruptedException e) {} 

		} finally {
       	Display.instance().close();
       	System.exit(0);
       }
	 }
}


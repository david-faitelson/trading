package edu.afeka.se.mvc.views.button;

import edu.afeka.se.mvc.Model;

public class ButtonModel extends Model {

	private boolean isPressed;
	
	public boolean isPressed() { return isPressed; }
	
	ButtonModel() {
		super();
		isPressed = false;
	}
	
	public void flip() {
		isPressed = !isPressed;
		notifyViews();
	}
}

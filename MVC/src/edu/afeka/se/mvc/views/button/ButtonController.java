package edu.afeka.se.mvc.views.button;

import edu.afeka.se.input.Mouse;
import edu.afeka.se.mvc.Controller;

public class ButtonController extends Controller {
	
	boolean respondedToPress = false;
	
	public void controlActivity() {
		super.controlActivity();
				
		if (Mouse.instance().button1Pressed() && !respondedToPress) {
			((ButtonModel)getModel()).flip();
			respondedToPress = true;
		}
		else if (!Mouse.instance().button1Pressed()) {
			respondedToPress = false;
		}
		
	}

}

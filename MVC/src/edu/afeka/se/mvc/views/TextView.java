package edu.afeka.se.mvc.views;

import java.awt.Color;

import edu.afeka.se.geom.Point;
import edu.afeka.se.geom.Rectangle;
import edu.afeka.se.graphics.Display;
import edu.afeka.se.mvc.View;

public class TextView extends View {

	private String text;
	
	public TextView(String text) {
		this.text = text;
	}
	
	public void displayView() {
		
		Rectangle rect = displayTransformation().applyTo(getWindow()).insetBy(10.0);

		Display.instance().text(rect.getLowerLeft(), text, Color.black);
		
	}
}

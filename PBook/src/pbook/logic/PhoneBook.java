package pbook.logic;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import pbook.storage.ContactDetailsInfo;
import pbook.storage.StorageManager;

public class PhoneBook {
	
	private Map<String, ContactDetails> contactDetails;
	private StorageManager storage;
	
	public PhoneBook() throws Exception {
		contactDetails = new HashMap<String, ContactDetails>();
		storage = new StorageManager();
		load();
	}
	
	public void addNewContact(String phoneNumber, String name) throws Exception {
		
		if (contactDetails.containsKey(name))
			throw new Exception("Name " + name + " already exists");
		
		contactDetails.put(name, new ContactDetails(phoneNumber, name));
		try {
			save();
		}
		catch(Exception err) {
			contactDetails.remove(name);
			throw err;
		}
	}
	
	public void removeContact(String name) throws Exception {
		
		ContactDetails contact = contactDetails.get(name);
		
		if (contact != null) {
			contactDetails.remove(name);
		}
		else 
			throw new Exception("Name " + name + " does not exist.");
		
		try {
			save();
		}
		catch(Exception err) {
			contactDetails.put(name, contact);
			throw err;
		}
	}
	
	public ContactDetails find(String name) {
		return contactDetails.get(name);
	}
	
	private void save() throws FileNotFoundException {
		
		List<ContactDetailsInfo> list = new LinkedList<ContactDetailsInfo>();
		for(ContactDetails contact : contactDetails.values()) {
			list.add(new ContactDetailsInfo(contact.getPhoneNumber(), contact.getName()));
		}
		storage.store(list);
	}

	private void load() throws FileNotFoundException {
		
		List<ContactDetailsInfo> list = storage.load();
			
		contactDetails.clear();
		for(ContactDetailsInfo info : list) {
			contactDetails.put(info.getName(), new ContactDetails(info));
		}
	}
}

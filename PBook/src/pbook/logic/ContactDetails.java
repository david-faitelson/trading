package pbook.logic;

import pbook.storage.ContactDetailsInfo;

public class ContactDetails {

	private String phoneNumber;
	private String name;
	
	public ContactDetails(String phoneNumber, String name) {
		this.phoneNumber = phoneNumber;
		this.name = name;
	}
	
	public ContactDetails(ContactDetailsInfo info) {
		this.name = info.getName();
		this.phoneNumber = info.getPhoneNumber();
	}

	public String getName() { return name; }
	
	public String getPhoneNumber() { return phoneNumber; }
}

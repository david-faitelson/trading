package pbook.storage;

import java.io.PrintStream;
import java.util.Scanner;

public class ContactDetailsInfo {
	private String phoneNumber;
	private String name;

	public ContactDetailsInfo(String phoneNumber, String name) {
		this.phoneNumber = phoneNumber;
		this.name = name;
	}

	public ContactDetailsInfo(Scanner scanner) {
		this.name = scanner.next();
		this.phoneNumber = scanner.next();
	}
	
	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public void writeInto(PrintStream out) {
		out.println(name + "," + phoneNumber);
	}
}

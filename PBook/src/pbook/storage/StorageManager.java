package pbook.storage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class StorageManager {

	private File file;

	public StorageManager() throws IOException {
		file = new File("pbook.data");
		
		if (!file.exists())
			Files.createFile(Paths.get("pbook.data"));
	}
	
	public void store(List<ContactDetailsInfo> list) throws FileNotFoundException {
		PrintStream out = new PrintStream(file);
		for(ContactDetailsInfo info : list) {
			info.writeInto(out);
		}
	}
	
	public List<ContactDetailsInfo> load() throws FileNotFoundException {
		List<ContactDetailsInfo> list = new LinkedList<ContactDetailsInfo>();
		
		Scanner scanner = new Scanner(new FileInputStream(file));
		
		scanner.useDelimiter("\\n|,");
		
		while (scanner.hasNext()) {
			list.add(new ContactDetailsInfo(scanner));
		}
		
		scanner.close();
		return list;
	}
}

package pbook.iface;

import java.io.IOException;
import java.util.Scanner;

import pbook.logic.ContactDetails;
import pbook.logic.PhoneBook;

public class PhoneBookApp {
	public static void main(String[] args) throws Exception {
		(new PhoneBookApp()).run(); 
	}
	
	public PhoneBookApp() throws Exception {
		
		scanner = new Scanner(System.in);
		
		pbook = new PhoneBook();
	}
	
	public void run() {
		
		showMenu();
		Integer code = selectOperation();
		
		while (code != 4) {
			try {
				switch(code) {
				case 1: addNewContact(); break;
				case 2: removeExistingContact(); break;
				case 3: lookForContact(); break;
				case 4: System.exit(0);
				default: 
					System.out.println("Invalid choice.");
				}
			}catch(Exception err) {
				System.out.println("Error: " + err.getMessage());
			}
			showMenu();
			code = selectOperation();
		}
	}
	
	private void lookForContact() {
		System.out.print("Enter name: ");
		String name = scanner.next();
		ContactDetails contact = pbook.find(name);
		if (contact == null)
			System.out.println("Sorry, this name is not in the phone book.");
		else {
			System.out.println("The phone number of " + name + " is " + contact.getPhoneNumber());
		}
	}

	private void removeExistingContact() {
		System.out.print("Enter name: ");
		String name = scanner.next();
		try {
			pbook.removeContact(name);
		}
		catch(IOException e) {
			System.out.println("I/O Error: " + e.getMessage());
		}
		catch (Exception e) {
			System.out.println("Sorry, this name does not exist in the phone book.");
		}
	}

	private void addNewContact() throws Exception {
		System.out.print("Enter name: ");
		String name = scanner.next();
		System.out.print("Enter phone number: ");
		String phoneNumber = scanner.next();
		try {
			pbook.addNewContact(phoneNumber, name);
		}
		catch(IOException e) {
			System.out.println("I/O Error: " + e.getMessage());
		}
		catch (Exception e) {
			System.out.println("Sorry, this name is already in the phone book.");
		}
	}

	private void showMenu() {
		System.out.println("1 - Add a new contact\n2 - Remove an existing contact\n3 - Find a contact by name\n4 - Exit.");
	}

	private Integer selectOperation() {
		Integer code = scanner.nextInt();
		while (code < 1 || code > 4)
		{
			System.out.println("Invalid choice. Try again (1-4)");
			code = scanner.nextInt();
		}
		return code;
	}
	
	private PhoneBook pbook;
	
	private Scanner scanner;
}

package exchange.server;

import java.io.IOException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import auth.Authenticator;

import auth.api.WrongSecretException;

import bank.api.BankManager;
import exchange.api.DoesNotHaveThisStockException;
import exchange.api.ExchangeManager;
import exchange.api.InternalExchangeErrorException;
import exchange.api.NoSuchAccountException;
import exchange.api.NotEnoughMoneyException;
import exchange.api.NotEnoughStockException;

import exchange.assets.iface.AssetsManagerController;
import exchange.trading.iface.TradingManagerController;
import exchange.trading.logic.Deposit;
import exchange.trading.logic.TradingManager;

public class ExchangeServer implements ExchangeManager, Runnable {

	private Authenticator authenticator;
	private TradingManagerController tradingController;
	private AssetsManagerController assetsController;
	
	static private final Integer EXCHANGE_BANK_ACCOUNT_ID = 3373;
	private static final String EXCHANGE_SECRET = "ZnqyTf"; 
	
    public static void main(String[] args) {
        try {
        	        	
        	if (args.length != 1) {
        		System.out.println("Usage: java exchange.server.ExchangeServer <port>");
        		System.exit(1);
        	}
        	
        	Integer port = Integer.parseInt(args[0]);

            String name = "Exchange";
            ExchangeServer server = new ExchangeServer();
            ExchangeManager stub =
                (ExchangeManager) UnicastRemoteObject.exportObject(server, port);
            Registry registry = LocateRegistry.getRegistry();
            registry.rebind(name, stub);
            System.out.println("The stock exchange is open.");
        } 
        catch (Exception e) {
            System.err.println("stock exchange server exception:");
            e.printStackTrace();
        }
    }
    
    public ExchangeServer() throws IOException, NotBoundException, SQLException, InternalExchangeErrorException, NoSuchAccountException {
    	    	
    	
    	BankManager bank = (BankManager) Naming.lookup("rmi://localhost/Bank");

    	this.authenticator = new Authenticator();
    	
    	this.assetsController = new AssetsManagerController(bank, EXCHANGE_SECRET, EXCHANGE_BANK_ACCOUNT_ID);
    	this.tradingController = new TradingManagerController(bank, EXCHANGE_SECRET, EXCHANGE_BANK_ACCOUNT_ID);
    	
    	new Thread(this).start();
    }
    
	@Override
	public Integer placeAsk(String secret, Integer accountId, String stockName, Integer amount, Integer price) 
			throws RemoteException, WrongSecretException, exchange.api.NoSuchAccountException, exchange.api.NotEnoughStockException, 
			exchange.api.StockNotTradedException, exchange.api.DoesNotHaveThisStockException, InternalExchangeErrorException {
		
    	if (authenticator.knowsPassword(accountId, secret)) {
    		    		
    		Integer available = assetsController.getAmountOfAsset(accountId, stockName); 
    		
    		if (available < amount) 
    			throw new NotEnoughStockException(accountId, stockName, available, amount);		
        	
    		Integer orderId = tradingController.placeAsk(accountId, stockName, amount, price);
    	
    		assetsController.decreaseAssets(accountId, stockName, amount);
    		
    		return orderId;
    	}
    	else
    		throw new WrongSecretException("placeAsk", accountId);
	}

	
	@Override
	public Integer placeBid(String secret, Integer accountId, String stockName, Integer amount, Integer price) 
			throws RemoteException, WrongSecretException, exchange.api.NoSuchAccountException, exchange.api.NotEnoughStockException, exchange.api.StockNotTradedException, InternalExchangeErrorException, NotEnoughMoneyException {

    	if (authenticator.knowsPassword(accountId, secret)) {
    		
    		Integer available = 0;
    		
			try {
				available = assetsController.getAmountOfAsset(accountId, TradingManager.CURRENCY);
			} catch (DoesNotHaveThisStockException e) { }
    		
    		if (available < amount*price)
    			throw new NotEnoughMoneyException(accountId, available, amount*price);
    	
    		Integer orderId = tradingController.placeBid(accountId, stockName, amount, price);

    		assetsController.decreaseAssets(accountId, TradingManager.CURRENCY, amount*price);
    		
    		return orderId;
    	}
    	else
    		throw new WrongSecretException("placeBid", accountId);
    }

	@Override
	public List<Integer> getOpenOrders(String secret, Integer accountId) 
			throws RemoteException, exchange.api.NoSuchAccountException, WrongSecretException, InternalExchangeErrorException {
		
    	if (authenticator.knowsPassword(accountId, secret)) {
    		
    		return tradingController.getOpenOrders(accountId);
    	}
    	else
    		throw new WrongSecretException("getOpenOrders", accountId);		
	}

	@Override
	public exchange.api.Order getOrderDetails(String secret, Integer accountId, Integer orderId) 
			throws RemoteException, WrongSecretException, exchange.api.NoSuchAccountException, InternalExchangeErrorException {
		
    	if (authenticator.knowsPassword(accountId, secret)) {
    		return tradingController.getOrderDetails(accountId, orderId);
    	}
    	else
    		throw new WrongSecretException("getOrderDetails", accountId);    	
	}


	@Override
	public Set<String> getAssets(String secret, Integer accountId)
			throws RemoteException, exchange.api.NoSuchAccountException, WrongSecretException, InternalExchangeErrorException {
				
    	if (authenticator.knowsPassword(accountId, secret)) {
    		
    		return new HashSet<String>(assetsController.getAssets(accountId));
    	}
    	else
    		throw new WrongSecretException("getAssets", accountId);    	
	}

	@Override
	public Integer getAmountOfAsset(String secret, Integer accountId, String assetName)
			throws RemoteException, exchange.api.NoSuchAccountException, WrongSecretException, InternalExchangeErrorException {
    	if (authenticator.knowsPassword(accountId, secret)) {
    		try {
				return assetsController.getAmountOfAsset(accountId,assetName);
			} catch (DoesNotHaveThisStockException e) {
				return 0;
			}
     	}
    	else
    		throw new WrongSecretException("getAmountOfAsset", accountId);    	
	}


/*
	@Override
	public void updateOrderPrice(String secret, Integer accountId, Integer orderId, Integer price) {
		
    	if (authenticator.knowsPassword(accountId, secret)) {
    		try {
    		}
    		catch() {
    			//throw new 
    		}
    	}
    	else
    		throw new WrongSecretException("updateOrderPrice", accountId);
	}

	@Override
	public void updateOrderAmount(String secret, Integer accountId, Integer orderId, Integer amount) {
		
    	if (authenticator.knowsPassword(accountId, secret)) {
    		try {
    		}
    		catch() {
    			//throw new 
    		}
    	}
    	else
    		throw new WrongSecretException("updateOrderAmount", accountId);
		
	}
*/
	
	@Override
	public Set<String> getStockNames() throws RemoteException {
		
			return tradingController.getTradedStocks();
	}

	@Override
	public Map<Integer, Integer> getSupply(String stockName) throws RemoteException {
		synchronized (this) {
			return tradingController.getSupply(stockName);
		}
	}

	@Override
	public Map<Integer, Integer> getDemand(String stockName) throws RemoteException {
		synchronized (this) {
			return tradingController.getDemand(stockName);
		}
	}

	@Override
	public Integer getExchangeBankAccountNumber() {
		return EXCHANGE_BANK_ACCOUNT_ID;
	}

	@Override
	public void run() {

		try {

			while(true) {
				
				for(Deposit each : tradingController.processOrders()) {
						try {
							assetsController.increaseAssets(each.getAccount(), TradingManager.CURRENCY, each.getAmount());
						} catch (NoSuchAccountException e) {
							System.out.println("Error (NoSuchAccountException): failed to increase the currency of account " + each.getAccount() + " by " + each.getAmount() + " units.");
						}
				}			
				try { Thread.sleep(1500); } catch(InterruptedException e) {}
			}
		} catch (InternalExchangeErrorException e) {
			System.out.println("Fatal Error (InternalExchangeErrorException):");
			e.printStackTrace();
			System.exit(1);
		}
	}
}

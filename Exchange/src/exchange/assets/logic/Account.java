package exchange.assets.logic;

import java.util.Map;
import java.util.Set;

import exchange.assets.storage.AssetTuple;
import exchange.assets.storage.Storage;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

public class Account {

	public Account(Integer bankAccId, List<AssetTuple> assetTuples) {
		
		bankAccountId = bankAccId;
		
		assets = new HashMap<String, Integer>();
		
		for(AssetTuple tuple : assetTuples) {
			assets.put(tuple.getAsset(), tuple.getAmount());
		}
	}

	public void increaseAssets(String assetName, Integer amount) throws SQLException {

		Integer currentAmount = assets.get(assetName);

		if (currentAmount == null) {
			Storage.instance().add(new AssetTuple(bankAccountId, assetName, amount));
			assets.put(assetName, amount);
		}
		else {
			Storage.instance().update(new AssetTuple(bankAccountId, assetName, amount+currentAmount));
			assets.put(assetName, amount + currentAmount);
		}
	}

	public void decreaseAssets(String assetName, Integer amount) throws SQLException {

		assert amount > 0;
	
		Integer currentAmount = assets.get(assetName);

		assert currentAmount >= amount;
		
		Storage.instance().update(new AssetTuple(bankAccountId, assetName, currentAmount - amount));
		assets.put(assetName, currentAmount - amount);
	}

	public Integer getBankAccountId() { return bankAccountId; }

	public Set<String> getAssets() {
		return assets.keySet();
	}

	public Integer getAmountOfAsset(String assetName) {
		return assets.get(assetName);
	}

	private Integer bankAccountId;
	private Map<String, Integer> assets;
	
}

package exchange.assets.logic;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import exchange.assets.storage.AssetTuple;
import exchange.assets.storage.Storage;

public class AssetsManager {

	private Map<Integer, Account> traderAccounts;

	public AssetsManager() {
		traderAccounts = new HashMap<Integer, Account>();
	}
	
	// asset information service
	
	public Set<String> getAssets(Integer accountId) throws NoSuchAccountException, SQLException {
				
		return new HashSet<String>(getAccount(accountId).getAssets());
	}
	
	public Integer getAmountOfAsset(Integer accountId, String assetName) throws NoSuchAccountException, SQLException {
		return getAccount(accountId).getAmountOfAsset(assetName);
	}
	
	private Account getAccount(Integer accountId) throws NoSuchAccountException, SQLException {
		
		Account account = traderAccounts.get(accountId);
			
		if (account == null) { // perhaps it was not yet loaded from storage
			
			List<AssetTuple> assets = Storage.instance().loadAccountAssets(accountId);
			
			if (!assets.isEmpty()) { // yes, there is such an account. create the object and fill it with assets
				account = new Account(accountId, assets); 
				traderAccounts.put(accountId, account);
			}
			else
				throw new NoSuchAccountException(accountId);
		}
		return account;
	}
	
	// asset update service (internal service provided to the trading subsystem
	
	public void increaseAssets(Integer accountId, String assetName, Integer amountToIncrease) throws SQLException, NoSuchAccountException {
		getAccount(accountId).increaseAssets(assetName, amountToIncrease);
	}

	public void decreaseAssets(Integer accountId, String assetName, Integer amountToDecrease) throws SQLException, NoSuchAccountException {
		getAccount(accountId).decreaseAssets(assetName, amountToDecrease);
	}
	
	public void commit() throws SQLException {
		Storage.instance().commit();
	}

	// latest deposits interface (internal service for synchronizing with the bank
	
	private Integer latestDepositId;
	
	public Integer getLatestDepositId() throws SQLException {
		if (latestDepositId == null)
			latestDepositId = Storage.instance().fetchLatestDepositId();
		return latestDepositId;
	}
	
	private void setLatestDepositId(Integer id) throws SQLException {
		if (id > latestDepositId) {
			Storage.instance().updateLatestDepositId(id);
			latestDepositId = id;
		}
	}

	public void acceptNewTraderDeposit(Integer source, String asset, Integer amount, Integer id) throws NoSuchAccountException, SQLException {
		Account account = getAccount(source);
		try {
			account.increaseAssets(asset, amount);
			setLatestDepositId(id);
			Storage.instance().commit();
		} catch(Exception e) {
			Storage.instance().abort();
			throw e;
		}
	}

}

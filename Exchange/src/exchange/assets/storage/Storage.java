package exchange.assets.storage;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class Storage {

	private static Storage storage;

	public static Storage instance() throws SQLException {
		if (storage == null)
			storage = new Storage();
		return storage;
	}

	private Connection conn;
	private PreparedStatement updateAssetStmt;
	private PreparedStatement insertAssetStmt;
	private PreparedStatement selectAssetsStmt;
	private PreparedStatement updateLatestDepositStmt;
	private PreparedStatement selectLatestDepositStmt;

	public Storage() throws SQLException {
		conn = DriverManager.getConnection("jdbc:derby:"+"EXCHANGE_ASSET_STORAGE");   
        conn.setAutoCommit(false);		
        
        updateAssetStmt = conn.prepareStatement("update ASSETS set amount = ? where account = ? and asset = ?");
        insertAssetStmt = conn.prepareStatement("insert into ASSETS values (?,?,?)");       
        selectAssetsStmt = conn.prepareStatement("select * from ASSETS where account = ?");
        
        updateLatestDepositStmt = conn.prepareStatement("update LATEST_DEPOSIT set transaction_id = ?");
        selectLatestDepositStmt = conn.prepareStatement("select transaction_id from LATEST_DEPOSIT");

	}
	
	public void add(AssetTuple assetTuple) throws SQLException {
		insertAssetStmt.setInt(1, assetTuple.getAccount());
		insertAssetStmt.setString(2, assetTuple.getAsset());
		insertAssetStmt.setInt(3, assetTuple.getAmount());
		insertAssetStmt.executeUpdate();
	}
	
	public void update(AssetTuple t) throws SQLException {

		updateAssetStmt.setInt(1, t.getAmount());
		updateAssetStmt.setInt(2, t.getAccount());
		updateAssetStmt.setString(3, t.getAsset());
		updateAssetStmt.executeUpdate();
	}	
	
	public void commit() throws SQLException {
		conn.commit();		
	}
	
	public void abort() throws SQLException {
		conn.rollback();
	}
	
	public List<AssetTuple> loadAccountAssets(Integer accountId) throws SQLException {
		
		selectAssetsStmt.setInt(1, accountId);
        
        ResultSet r = null;
        try {
            r = selectAssetsStmt.executeQuery();
        
            List<AssetTuple> rs = new LinkedList<AssetTuple>();
        
            while (r.next())
                rs.add(new AssetTuple(r));
            
            return rs;
            
        } finally {
            if (r != null)
                r.close();
        }
	}
	
	public void updateLatestDepositId(Integer id) throws SQLException {
		updateLatestDepositStmt.setInt(1, id);
		updateLatestDepositStmt.executeUpdate();
	}
	
	public Integer fetchLatestDepositId() throws SQLException {
		
		ResultSet r = null;
		
		try {
			r = selectLatestDepositStmt.executeQuery();
			r.next();
			return r.getInt(1);
		} 
		finally {
			if (r != null)
				r.close();
		}
	}

}

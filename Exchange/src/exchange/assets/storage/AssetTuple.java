package exchange.assets.storage;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AssetTuple {

	private Integer account;
	private String asset;
	private Integer amount;

	public AssetTuple(Integer identifier, String asset, Integer amount) {
		this.account = identifier;
		this.asset = asset;
		this.amount = amount;
	}

	public AssetTuple(ResultSet r) throws SQLException {
		this.account = r.getInt("account");
		this.asset = r.getString("asset");
		this.amount = r.getInt("amount"); 
	}

	public Integer getAccount() {
		return account;
	}

	public void setAccount(Integer account) {
		this.account = account;
	}

	public String getAsset() {
		return asset;
	}

	public void setAsset(String asset) {
		this.asset = asset;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

}

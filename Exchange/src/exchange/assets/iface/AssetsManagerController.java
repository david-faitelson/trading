package exchange.assets.iface;

import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import auth.api.WrongSecretException;
import bank.api.BankManager;
import bank.api.InternalServerErrorException;
import exchange.api.DoesNotHaveThisStockException;
import exchange.api.InternalExchangeErrorException;
import exchange.assets.logic.AssetsManager;
import exchange.assets.logic.NoSuchAccountException;

public class AssetsManagerController implements Runnable {
	
	private BankManager bank;
	private AssetsManager assets;
	private String exchangeBankAccountSecret;
	private Integer exchangeBankAccountId;

	public AssetsManagerController(BankManager bank, String secret, Integer id) {

		this.bank = bank;
		this.assets = new AssetsManager();

		this.exchangeBankAccountSecret = secret;
		this.exchangeBankAccountId = id;
		
		new Thread(this).start();
	}
	
	// asset information service
	
	public Set<String> getAssets(Integer accountId)
			throws RemoteException, exchange.api.NoSuchAccountException, WrongSecretException, InternalExchangeErrorException {
				
		try {
			synchronized (this) {
				return new HashSet<String>(assets.getAssets(accountId));
			}
		}
		catch(exchange.assets.logic.NoSuchAccountException e) {
			throw new exchange.api.NoSuchAccountException(e.getBankAccountId()); 
		} catch (SQLException e) {
			throw new exchange.api.InternalExchangeErrorException(e.getMessage());
		}
	}

	public Integer getAmountOfAsset(Integer accountId, String assetName)
			throws RemoteException, exchange.api.NoSuchAccountException, WrongSecretException, InternalExchangeErrorException, DoesNotHaveThisStockException {

		try {
			synchronized (this) {
				
				Integer available = assets.getAmountOfAsset(accountId, assetName); 
				
				if (available == null)
					throw new DoesNotHaveThisStockException(accountId, assetName);

				return available;
			}
		}
		catch(exchange.assets.logic.NoSuchAccountException e) {
			throw new exchange.api.NoSuchAccountException(e.getBankAccountId()); 
		} catch (SQLException e) {
			throw new exchange.api.InternalExchangeErrorException(e.getMessage());
		}
	}

	// asset update service
	
	public void increaseAssets(Integer accountId, String assetName, Integer amountToIncrease) throws InternalExchangeErrorException, exchange.api.NoSuchAccountException {
		try {
			synchronized (this) {
				assets.increaseAssets(accountId,assetName,amountToIncrease);
				assets.commit();
			}
		}
		catch (SQLException e) {
			throw new exchange.api.InternalExchangeErrorException(e.getMessage());
		} catch (NoSuchAccountException e) {
			throw new exchange.api.NoSuchAccountException(e.getBankAccountId()); 
		}
	
	}

	public void decreaseAssets(Integer accountId, String assetName, Integer amountToDecrease) throws InternalExchangeErrorException, exchange.api.NoSuchAccountException {
		try {
			synchronized (this) {
				assets.decreaseAssets(accountId,assetName,amountToDecrease);
				assets.commit();
			}
		}
		catch (SQLException e) {
			throw new exchange.api.InternalExchangeErrorException(e.getMessage());
		} catch (NoSuchAccountException e) {
			throw new exchange.api.NoSuchAccountException(e.getBankAccountId()); 
		}
	}
	// latest deposits service
	
	private void fetchNewDeposits() throws WrongSecretException, RemoteException, InternalServerErrorException, SQLException {
		
		List<bank.api.Transaction> transactions = bank.getDepositsAfter(exchangeBankAccountSecret, exchangeBankAccountId, assets.getLatestDepositId());
		
		for(bank.api.Transaction t : transactions) {
			
				synchronized (this) {
					try {
						assets.acceptNewTraderDeposit(t.getSource(), t.getAsset(), t.getAmount(), t.getId());
					} catch (NoSuchAccountException e) {
						System.out.println("Warning: accepted a deposit of " + t.getAmount() + " " + t.getAsset() + "units for non existing account " + t.getSource());
					}
				}
		}
		
	}

	@Override
	public void run() {
		
		while (true) {
			try {
				
				fetchNewDeposits(); 
				
				try { Thread.sleep(1000); } catch (InterruptedException e) { }// just ignore it

			} catch (RemoteException e) {
				System.out.println("Warning (RemoteException): cannot contact the bank. Retrying in 5 seconds.");
				try { Thread.sleep(5000); } catch(InterruptedException err) {};
			} catch (InternalServerErrorException e) {
				System.out.println("Warning (InternalServerErrorException): the bank server had an internal error: " + e.getMessage());
			} catch (WrongSecretException e) {
				System.out.println("Fatal Error (WrongSecretException): the secret for the stock exchange account in the bank is wrong.");
				e.printStackTrace();
				System.exit(1);
			} catch (SQLException e) {
				System.out.println("Fatal Error (SQLException): ");
				e.printStackTrace();
				System.exit(1);
			}
		}
	}
}

package exchange.trading.iface;

import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import auth.api.WrongSecretException;

import bank.api.BankManager;
import bank.api.DoesNotHaveThisAssetException;
import bank.api.InternalServerErrorException;
import bank.api.NotEnoughAssetException;
import exchange.api.InternalExchangeErrorException;
import exchange.api.NotEnoughMoneyException;
import exchange.trading.logic.Deposit;
import exchange.trading.logic.NoSuchAccountException;
import exchange.trading.logic.TradingManager;
import exchange.trading.logic.Transaction;

public class TradingManagerController {

	private TradingManager tradingManager;
	private BankManager bank;
	
	private String exchangeBankAccountSecret;
	private Integer exchangeBankAccountId;

    
    public TradingManagerController(BankManager bank, String secret, Integer id) throws IOException, NotBoundException, SQLException, InternalExchangeErrorException, exchange.api.NoSuchAccountException {
    	
    	try {
			this.tradingManager = new TradingManager();
		} catch (NoSuchAccountException e) {
			throw new exchange.api.NoSuchAccountException(e.getBankAccountId());
		}
    	
    	this.bank = bank;
    	    	
    	this.exchangeBankAccountId = id;
    	
    	this.exchangeBankAccountSecret = secret;

    }
    
	public Integer placeAsk(Integer accountId, String stockName, Integer amount, Integer price) 
			throws RemoteException, WrongSecretException, exchange.api.NoSuchAccountException, exchange.api.NotEnoughStockException, 
			exchange.api.StockNotTradedException, exchange.api.DoesNotHaveThisStockException, InternalExchangeErrorException {
		
    		try {
    			synchronized (this) {
    				return tradingManager.placeAsk(accountId, stockName, amount, price, new Date());
    			}
    		}
    		catch(exchange.trading.logic.NoSuchAccountException e) {
    			throw new exchange.api.NoSuchAccountException(e.getBankAccountId());
    		}
    		catch(exchange.trading.logic.NotEnoughAssetException e) {
    			throw new exchange.api.NotEnoughStockException(e.getAccount().getBankAccountId(), e.getAsset(), e.getCurrentAmount(), e.getRequestedAmount());
    		}
    		catch(exchange.trading.logic.StockNotTradedException e) {
    			throw new exchange.api.StockNotTradedException(e.getStockName());
    		} 
    		catch (exchange.trading.logic.DoesNotHaveThisAssetException e) {
    			throw new exchange.api.DoesNotHaveThisStockException(e.getAccount().getBankAccountId(), e.getAsset());
			} catch (SQLException e) {
				throw new exchange.api.InternalExchangeErrorException(e.getMessage());
			}
	}

	public Integer placeBid(Integer accountId, String stockName, Integer amount, Integer price) 
			throws RemoteException, WrongSecretException, exchange.api.NoSuchAccountException, exchange.api.NotEnoughStockException, exchange.api.StockNotTradedException, InternalExchangeErrorException, NotEnoughMoneyException {

			try {
    			synchronized (this) {
    				return tradingManager.placeBid(accountId, stockName, amount, price, new Date());
    			}
			} 
			catch (exchange.trading.logic.NoSuchAccountException e) {
				throw new exchange.api.NoSuchAccountException(e.getBankAccountId());
			} 
			catch (exchange.trading.logic.NotEnoughAssetException e) {
				if (e.getAsset().equals(TradingManager.CURRENCY))
					throw new exchange.api.NotEnoughMoneyException(e.getAccount().getBankAccountId(), e.getCurrentAmount(), e.getRequestedAmount());
				else
					throw new exchange.api.NotEnoughStockException(e.getAccount().getBankAccountId(), e.getAsset(), e.getCurrentAmount(), e.getRequestedAmount());
			} 
			catch (exchange.trading.logic.StockNotTradedException e) {
				throw new exchange.api.StockNotTradedException(e.getStockName());
			} catch (SQLException e) {
				throw new exchange.api.InternalExchangeErrorException(e.getMessage());
			}
    }

	public List<Integer> getOpenOrders(Integer accountId) 
			throws RemoteException, exchange.api.NoSuchAccountException, WrongSecretException, InternalExchangeErrorException {
		
		try {
			synchronized (this) {
				return tradingManager.getAccount(accountId).getOpenOrders();
			}
		}
		catch(exchange.trading.logic.NoSuchAccountException e) {
			throw new exchange.api.NoSuchAccountException(e.getBankAccountId()); 
		} catch (SQLException e) {
			throw new exchange.api.InternalExchangeErrorException(e.getMessage());
		}
	}

	public exchange.api.Order getOrderDetails(Integer accountId, Integer orderId) 
			throws RemoteException, WrongSecretException, exchange.api.NoSuchAccountException, InternalExchangeErrorException {
		
		exchange.trading.logic.Order order = null;
		synchronized (this) {
			try {
				order = tradingManager.getOrderDetails(accountId, orderId);
			} catch (NoSuchAccountException e) {
				throw new exchange.api.NoSuchAccountException(e.getBankAccountId());
			} catch (SQLException e) {
				throw new InternalExchangeErrorException(e.getMessage());
			}
		}
		return new exchange.api.Order(order.getId(), order.getKind(), order.getStockName(), order.getAmount(), order.getPrice(), order.getDate());
	}



/*
	@Override
	public void updateOrderPrice(String secret, Integer accountId, Integer orderId, Integer price) {
		
    	if (authenticator.knowsPassword(accountId, secret)) {
    		try {
    		}
    		catch() {
    			//throw new 
    		}
    	}
    	else
    		throw new WrongSecretException("updateOrderPrice", accountId);
	}

	@Override
	public void updateOrderAmount(String secret, Integer accountId, Integer orderId, Integer amount) {
		
    	if (authenticator.knowsPassword(accountId, secret)) {
    		try {
    		}
    		catch() {
    			//throw new 
    		}
    	}
    	else
    		throw new WrongSecretException("updateOrderAmount", accountId);
		
	}
*/
	
	public Set<String> getTradedStocks() throws RemoteException {
		synchronized (this) {
			return tradingManager.getTradedStocks();
		}
	}

	public Map<Integer, Integer> getSupply(String stockName) throws RemoteException {
		synchronized (this) {
			return tradingManager.getSupply(stockName);
		}
	}

	public Map<Integer, Integer> getDemand(String stockName) throws RemoteException {
		synchronized (this) {
			return tradingManager.getDemand(stockName);
		}
	}

	private void transferAssets(Integer destinationAccount, String assetName, Integer amount) {

		try {
			boolean succeeded = false;
			
			while (!succeeded) {
				try {
					bank.transferAssets(exchangeBankAccountSecret, exchangeBankAccountId, destinationAccount, assetName, amount);
					succeeded = true;
				} catch(RemoteException e) {
					System.out.println("Warning (RemoteException): Could not transfer assets to bank. Will try again in 5 sec.");
					try { Thread.sleep(5000); } catch (InterruptedException e1) {}
				}
			}
		} catch(WrongSecretException e) {
				System.out.println("Fatal Error (WrongSecretException): Could not access exchange's bank account.");
				e.printStackTrace();
				System.exit(1);
		} catch (DoesNotHaveThisAssetException e) {
			System.out.println("Error (DoesNotHaveThisAssetException): Failed to transfer " + amount + " units of asset " + assetName + " to account " + destinationAccount + ". The stock exchange does not hold this asset." );
		} catch (NotEnoughAssetException e) {
			System.out.println("Error (DoesNotHaveThisAssetException): Failed to transfer " + amount + " units of asset " + assetName + " to account " + destinationAccount + ". The stock exchange does holds only " + e.getCurrentAmount() + " units of this asset." );
		} catch (InternalServerErrorException e) {
			System.out.println("Fatal Error (InternalServerErrorException): The bank server had an internal error: ");
			e.printStackTrace();
			System.exit(1);
		}
	}
	
	public List<Deposit> processOrders() throws InternalExchangeErrorException {
		
		List<Deposit> deposits = new LinkedList<Deposit>();
		
			List<Transaction> transactions = null;
			
			synchronized (this) {
				 try {
					transactions = tradingManager.matchOpenOrders(deposits);
				} catch (SQLException e) {
					throw new InternalExchangeErrorException(e.getMessage());
				}
			}
		
			for(Transaction tx : transactions) {
					
					/*	bank transfer:amountToTransfer unitsOf:anAsk stock from: exchangeBankAccount to: aBid bidder.
					bank transfer:amountToTransfer * aBid price unitsOf:#USD from:exchangeBankAccount to: anAsk asker
				*/
					
				transferAssets(tx.getBidderBankAccountId(), tx.getStockName(), tx.getAmountToTransfer());
				transferAssets(tx.getAskerBankAccountId(), TradingManager.CURRENCY, tx.getAmountToTransfer()*tx.getStockPrice());
			}
			
			synchronized (this) {
				tradingManager.clearCompletedOrders();
			}
			
			return deposits;
	}
}

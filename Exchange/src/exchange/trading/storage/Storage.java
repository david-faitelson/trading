package exchange.trading.storage;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

public class Storage {

	private static Storage storage;

	public static Storage instance() throws SQLException {
		if (storage == null)
			storage = new Storage();
		return storage;
	}

	private Connection conn;
	private PreparedStatement insertOrderStmt;
	private PreparedStatement updateOrderStmt;
	private PreparedStatement selectOrdersStmt;
	private PreparedStatement selectTradedStocksStmt;

	public Storage() throws SQLException {
		conn = DriverManager.getConnection("jdbc:derby:"+"EXCHANGE_TRADING_STORAGE");   
        conn.setAutoCommit(false);		
        
        /* create table OPEN_ORDERS (id int, kind char, stock varchar(20), amount int, price int, creation_date timestamp, account int);
        */

        insertOrderStmt = conn.prepareStatement("insert into OPEN_ORDERS (kind,stock,amount,price,creation_date,account) values (?,?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);    
        updateOrderStmt = conn.prepareStatement("update OPEN_ORDERS set amount = ?, price = ? where id = ?");
        selectOrdersStmt = conn.prepareStatement("select * from OPEN_ORDERS where amount > 0 order by creation_date asc");
        
        selectTradedStocksStmt = conn.prepareStatement("select * from STOCKS");
	}

	public Integer add(OrderTuple t) throws SQLException {
		
		insertOrderStmt.setString(1, t.getKind());
		insertOrderStmt.setString(2, t.getStockName());
		insertOrderStmt.setInt(3, t.getAmount());
		insertOrderStmt.setInt(4, t.getPrice());
		insertOrderStmt.setTimestamp(5, new Timestamp(t.getDate().getTime()));
		insertOrderStmt.setInt(6, t.getAccount());
		
		insertOrderStmt.executeUpdate();
		
		ResultSet r = null;
		
		try {
			r = insertOrderStmt.getGeneratedKeys();
			r.next(); 
			return r.getInt(1);
		} finally {
			if (r != null)
				r.close();
		}
	}

	public List<OrderTuple> loadOpenOrders() throws SQLException {
        
        ResultSet r = null;
        try {
            r = selectOrdersStmt.executeQuery();
        
            List<OrderTuple> rs = new LinkedList<OrderTuple>();
        
            while (r.next())
                rs.add(new OrderTuple(r));
            
            return rs;
            
        } finally {
            if (r != null)
                r.close();
        }
		
	}
	
	public List<String> getTradedStocks() throws SQLException {
		
	     ResultSet r = null;
	        try {
	            r = selectTradedStocksStmt.executeQuery();
	        
	            List<String> rs = new LinkedList<String>();
	        
	            while (r.next())
	                rs.add(r.getString("name"));
	            
	            return rs;
	            
	        } finally {
	            if (r != null)
	                r.close();
	        }
		}
	
	public void commit() throws SQLException {
		conn.commit();		
	}
	
	public void abort() throws SQLException {
		conn.rollback();
	}
	
	public void update(OrderTuple t) throws SQLException {
		updateOrderStmt.setInt(1, t.getAmount());
		updateOrderStmt.setInt(2, t.getPrice());
		updateOrderStmt.setInt(3,  t.getId());
		updateOrderStmt.executeUpdate();
	}
}

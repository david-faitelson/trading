package exchange.trading.storage;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/* create table OPEN_ORDERS (id int, kind char, stock varchar(20), amount int, price int, date timestamp, account int);
*/
public class OrderTuple {
	
	public static final String BID = "B";
	public static final String ASK = "A";
	
	private Integer id;
	private String kind;
	private String stockName;
	private Integer amount;
	private Integer price;
	private Date date;
	private Integer account;

	public OrderTuple(Integer id, String kind, String stockName, Integer amount, Integer price, Date date, Integer account) {
		this.id = id;
		this.kind = kind;
		this.stockName = stockName;
		this.amount = amount;
		this.price = price;
		this.date = date;
		this.account = account;
	}

	public OrderTuple(ResultSet r) throws SQLException {
		this.id = r.getInt("id");
		this.kind = r.getString("kind");
		this.stockName = r.getString("stock");
		this.amount = r.getInt("amount");
		this.price = r.getInt("price");
		this.date = r.getDate("creation_date");
		this.account = r.getInt("account");
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getKind() {
		return kind;
	}

	public void setKind(String kind) {
		this.kind = kind;
	}

	public String getStockName() {
		return stockName;
	}

	public void setStockName(String stockName) {
		this.stockName = stockName;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Integer getAccount() {
		return account;
	}

	public void setAccount(Integer account) {
		this.account = account;
	}
}

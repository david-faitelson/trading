package exchange.trading.logic;

public class StockNotTradedException extends Exception {
	private String stockName;

	public StockNotTradedException(String stockName) {
		this.stockName = stockName;
	}

	public String getStockName() {
		return stockName;
	}

}

package exchange.trading.logic;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import exchange.trading.storage.OrderTuple;
import exchange.trading.storage.Storage;

public class Bid extends Order {

	public Bid(Integer id, Account bd, String name, Integer amount, Integer price, Date date) {
		super(id, name, amount, price,date);
		bidder = bd;
	}

	public Account getBidder() { return bidder; }

	public Collection<? extends Transaction> matchIn(TradingManager exchange, List<Deposit> deposits) throws SQLException {
		return exchange.matchBid(this, deposits);
	}

	public String getKind() { return "B"; }
	
	@Override
	public void addToStorage() throws SQLException {
		setId(Storage.instance().add(new OrderTuple(null, getKind(), getStockName(), getAmount(), getPrice(), getDate(), bidder.getBankAccountId())));
	}

	private Account bidder;

	public void updateStorage() throws SQLException {
		Storage.instance().update(new OrderTuple(getId(), getKind(), getStockName(), getAmount(), getPrice(), getDate(), bidder.getBankAccountId()));
		
	}
	
	@Override
	public void uploadInto(TradingManager exchange) {
		exchange.upload(this);
	}

}

package exchange.trading.logic;

public class Deposit {
	
	private Integer account;
	private Integer amount;

	public Deposit(Integer account, Integer amount) {
		this.account = account;
		this.setAmount(amount);
	}

	public Integer getAccount() {
		return account;
	}

	public void setAccount(Integer account) {
		this.account = account;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}
	
	
}

package exchange.trading.logic;

import java.util.List;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

import exchange.trading.logic.Order;
import exchange.trading.storage.OrderTuple;
import exchange.trading.storage.Storage;

public class TradingManager {

	public static final String CURRENCY = "NIS";
	
	public TradingManager() throws SQLException, NoSuchAccountException {
		
		traderAccounts = new HashMap<Integer, Account>();
		openOrders = new LinkedList<Order>();
		supply = new HashMap<String, List<Ask> >();
		demand = new HashMap<String, List<Bid> >();
		tradedStocks = new HashSet<String>();
			
		tradedStocks.addAll(Storage.instance().getTradedStocks());
		
		/* RONALDO MESSI NEYMAR SUAREZ BALE OZIL PEPE ZEHAVI BARDA BENAYUN */
		
		// ensure that each traded stock has a list of supply & demand orders
		
		for(String stockName : tradedStocks) {
			supply.put(stockName, new LinkedList<Ask>());
		}
		
		for(String stockName : tradedStocks) {
			demand.put(stockName, new LinkedList<Bid>());
		}
		
		for(OrderTuple t : Storage.instance().loadOpenOrders()) {
			
			Account account = getAccount(t.getAccount());
			
			Order order = account.upload(t);// load the order into the list of orders for its account
			openOrders.add(0, order); // add the order to the list of open orders
			order.uploadInto(this); // add the order to the supply or demand maps according to its type
		}
	}
		
	public void upload(Bid bid) {
		demand.get(bid.getStockName()).add(0, bid);
	}
	
	public void upload(Ask ask) {
		supply.get(ask.getStockName()).add(0, ask);
	}
	
	public Integer placeAsk(Integer askerId, String stockName, Integer amount, Integer price, Date date) throws NoSuchAccountException, DoesNotHaveThisAssetException, NotEnoughAssetException, StockNotTradedException, SQLException {

		if (amount <= 0)
			throw new IllegalArgumentException("Amount must be at least 1");
		
		if (price <= 0)
			throw new IllegalArgumentException("Price must be at least 1");

		if (!tradedStocks.contains(stockName))
			throw new IllegalArgumentException("Stock " + stockName + " is not traded in the exchange.");
		
		return placeAsk(new Ask(null, getAccount(askerId), stockName, amount, price, date));
	}

	public Integer placeBid(Integer bidderId, String stockName, Integer amount, Integer price, Date date) throws NoSuchAccountException, NotEnoughAssetException, StockNotTradedException, SQLException {
		
		if (amount <= 0)
			throw new IllegalArgumentException("Amount must be at least 1");
		
		if (price <= 0)
			throw new IllegalArgumentException("Price must be at least 1");
		
		if (!tradedStocks.contains(stockName))
			throw new IllegalArgumentException("Stock " + stockName + " is not traded in the exchange.");

		return placeBid(new Bid(null, getAccount(bidderId), stockName, amount, price, date));
	}

	private Integer placeAsk(Ask anAsk) throws NoSuchAccountException, DoesNotHaveThisAssetException, NotEnoughAssetException, StockNotTradedException, SQLException {
		/*
		 * placeAsk: anAsk

	| supplyOfStock |
	
	(traderAccounts at:anAsk asker) placeAsk:anAsk.
	anAsk date:DateAndTime now.
	openOrders add:anAsk.

	supplyOfStock _ supply at:anAsk stock 
		ifAbsentPut:[SortedCollection new sort:[:x :y | (x price < y price) or:[x price = y price and:[x date <= y date]]]].

	supplyOfStock add:anAsk
		 */
	
		Account asker = getAccount(anAsk.getAsker().getBankAccountId());
		
		if (asker == null)
			throw new NoSuchAccountException(anAsk.getAsker().getBankAccountId());
		 
		if (!tradedStocks.contains(anAsk.getStockName()))
				throw new StockNotTradedException(anAsk.getStockName());
		
		anAsk.addToStorage();
		
		asker.placeAsk(anAsk);
		openOrders.add(0, anAsk);
		
		supply.get(anAsk.getStockName()).add(0, anAsk);
		
		Storage.instance().commit();
		
		return anAsk.getId();
	}
	
	private Integer placeBid(Bid aBid) throws NoSuchAccountException, NotEnoughAssetException, StockNotTradedException, SQLException {
		/*
		placeBid: aBid

		| demandForStock |
		
		(traderAccounts at:aBid bidder) placeBid: aBid.
		aBid date:DateAndTime now.
		openOrders add:aBid.
		
		demandForStock _ demand at:aBid stock 
			ifAbsentPut:[SortedCollection new sort:[:x :y | (x price > y price) or:[x price = y price and:[x date <= y date]]]].

		demandForStock add:aBid
		*/
		
		Account bidder = getAccount(aBid.getBidder().getBankAccountId());
		
		if (bidder == null)
			throw new NoSuchAccountException(aBid.getBidder().getBankAccountId());

		if (!tradedStocks.contains(aBid.getStockName()))
			throw new StockNotTradedException(aBid.getStockName());

		aBid.addToStorage();
		
		bidder.placeBid(aBid);
		
		openOrders.add(0, aBid);
		
		demand.get(aBid.getStockName()).add(0, aBid);

		Storage.instance().commit();

		return aBid.getId();
	}
	
	private Transaction doTheDeal(Integer amountToTransfer, Bid aBid, Ask anAsk, List<Deposit> deposits) throws SQLException {
		/*
		 * transfer: amountToTransfer between: aBid and:anAsk

	"transfer the amount to transfer of stocks to the bidder's bank account, and the asked amount of money (the ask's price) to the asker's bank account. In addition, update the bid and the ask to reflect the remaining amounts."
	
	self assert:[aBid stock = anAsk stock].
	self assert:[aBid amount >= amountToTransfer].
	self assert:[anAsk amount >= amountToTransfer].
	
	aBid amount: aBid amount - amountToTransfer.
	anAsk amount: anAsk amount - amountToTransfer.
			
	" If the deal is cheaper than what the bidder was willing to pay, we return the extra money back to the bidder's exchange account. "
	(aBid price > anAsk price) ifTrue:
		[(traderAccounts at:aBid bidder) add: (aBid price - anAsk price) * amountToTransfer unitsOf:#USD].
	
	bank transfer:amountToTransfer unitsOf:anAsk stock from: exchangeBankAccount to: aBid bidder.
	bank transfer:amountToTransfer * aBid price unitsOf:#USD from:exchangeBankAccount to: anAsk asker

		 */
		assert aBid.getStockName().equals(anAsk.getStockName());
				
		aBid.decreaseAmountBy(amountToTransfer);
		anAsk.decreaseAmountBy(amountToTransfer);

		aBid.updateStorage();
		anAsk.updateStorage();
		Storage.instance().commit();
		
		/*	If the deal is cheaper than what the bidder was willing to pay, we return the extra money back to the bidder's exchange account.  */
		
		if (aBid.getPrice() > anAsk.getPrice()) {
			deposits.add(new Deposit(aBid.getBidder().getBankAccountId(), aBid.getPrice() - anAsk.getPrice() * amountToTransfer));
		}
		
		return new Transaction(amountToTransfer, aBid.getPrice(), anAsk.getStockName(), aBid.getBidder().getBankAccountId(), anAsk.getAsker().getBankAccountId());
	}
	
	List<Transaction> matchAsk(Ask anAsk, List<Deposit> deposits) throws SQLException {
		
		/*
		 * matchAsk: anAsk

	((demand at:anAsk stock) select:[:eachBid | eachBid price >= anAsk price]) do: [: eachBid | 
			((anAsk amount > 0) and:[eachBid amount > 0]) 
				ifTrue:[self transfer:(anAsk amount min: eachBid amount) between:eachBid and: anAsk]]

	
		 */
		
		List<Transaction> transactions = new LinkedList<Transaction>();
		
		for(Bid bid : demand.get(anAsk.getStockName())) {
			if (bid.getPrice() >= anAsk.getPrice() && anAsk.getAmount() > 0 && bid.getAmount() > 0)
				transactions.add(doTheDeal(Math.min(anAsk.getAmount(),bid.getAmount()), bid, anAsk, deposits));				
		}
		
		return transactions;
	}
	
	List<Transaction> matchBid(Bid aBid, List<Deposit> deposits) throws SQLException {
		
		/*
		 * matchBid: aBid

	((supply at:aBid stock) select:[:eachAsk | eachAsk price <= aBid price]) do: [: eachAsk | 
			((aBid amount > 0) and:[eachAsk amount > 0]) 
				ifTrue:[self transfer:(aBid amount min: eachAsk amount) between:aBid and: eachAsk]]

		 */
		
		List<Transaction> transactions = new LinkedList<Transaction>();
		
		for(Ask ask : supply.get(aBid.getStockName())) {
			if (ask.getPrice() <= aBid.getPrice() && ask.getAmount() > 0 && aBid.getAmount() > 0)
				transactions.add(doTheDeal(Math.min(aBid.getAmount(),ask.getAmount()), aBid, ask, deposits));				
		}
		
		return transactions;
	}
	
	public List<Transaction> matchOpenOrders(List<Deposit> deposits) throws SQLException {
		
		List<Transaction> transactions = new LinkedList<Transaction>();
		for(Order order : openOrders)
			transactions.addAll(order.matchIn(this, deposits));
		
		return transactions;
	}
	
	public void clearCompletedOrders() {
		
		/*
		 * logCompleteOrders
	| completeOrders | 
	
	completeOrders _ openOrders select:[:each | each amount = 0].
	
	openOrders removeAll:completeOrders.
	
	traderAccounts do:[:each | each clearEmptyOrders ].
	
	supply do:[ :each | each removeAllFoundIn: completeOrders ].
	demand do:[:each | each removeAllFoundIn: completeOrders ].
	
	transactionLog addAll:completeOrders.  
	
		 */
		
		List<Order> completeOrders = new LinkedList<Order>();
		
		for(Order order : openOrders)
			if (order.getAmount() == 0)
				completeOrders.add(order);
		
		openOrders.removeAll(completeOrders);
		
		for(Account account : traderAccounts.values())
			account.clearEmptyOrders();
		
		for(List<Ask> asks : supply.values())
			asks.removeAll(completeOrders);
		
		for(List<Bid> bids : demand.values())
			bids.removeAll(completeOrders);
	}
	
	public Account getAccount(Integer accountId) throws NoSuchAccountException, SQLException {
		
		Account account = traderAccounts.get(accountId);
			
		if (account == null) { // it was not yet loaded from storage
			
				account = new Account(accountId);
				traderAccounts.put(accountId, account);
		}
		
		return account;
	}

	public Map<Integer, Integer> getSupply(String stockName) {
		
		return getAmountPerPrice(supply.get(stockName));
	}

	public Map<Integer, Integer> getDemand(String stockName) {
		
	
		return getAmountPerPrice(demand.get(stockName));
	}

	private Map<Integer, Integer> getAmountPerPrice(Collection<? extends Order> collection) {
		
		Map<Integer, Integer> priceAmountMap = new HashMap<Integer, Integer>();
		
		if (collection != null) {
			
			for(Order eachOrder : collection) {
				
				Integer amountAtThisPrice = priceAmountMap.get(eachOrder.getPrice());
				if (amountAtThisPrice == null)
					amountAtThisPrice = 0;
				priceAmountMap.put(eachOrder.getPrice(), amountAtThisPrice + eachOrder.getAmount());
			}
		}
		
		return priceAmountMap;
	}
	
	public Set<String> getTradedStocks() { return tradedStocks; }
	
	private Map<Integer, Account> traderAccounts;
	private List<Order> openOrders;
	private Map<String, List<Ask> > supply;
	private Map<String, List<Bid> > demand;
	private Set<String> tradedStocks;
	

	public Order getOrderDetails(Integer accountId, Integer orderId) throws NoSuchAccountException, SQLException {
		return getAccount(accountId).getOrder(orderId);
	}
}

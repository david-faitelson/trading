package exchange.trading.logic;

public class NotEnoughAssetException extends Exception {
	private Account account;
	private String asset;
	private Integer currentAmount;
	private Integer requestedAmount;

	public NotEnoughAssetException(Account account, String asset, Integer currentAmount, Integer requestedAmount) {

		this.account = account;
		this.asset = asset;
		this.currentAmount = currentAmount;
		this.requestedAmount= requestedAmount;
	}

    public Account getAccount() { return account; }
    public String getAsset() { return asset; }
    public Integer getCurrentAmount() { return currentAmount; }
    public Integer getRequestedAmount() { return requestedAmount; }

}

package exchange.trading.logic;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import exchange.trading.storage.OrderTuple;
import exchange.trading.storage.Storage;

public class Ask extends Order {

	public Ask(Integer id, Account askr, String name, Integer amount, Integer price, Date date) {
		super(id, name, amount, price,date);
		asker = askr;
	}

	public Account getAsker() { return asker; }

	public Collection<? extends Transaction> matchIn(TradingManager exchange, List<Deposit> deposits) throws SQLException {
		return exchange.matchAsk(this, deposits);
	}

	public String getKind() { return "A"; }
	
	private Account asker;

	@Override
	public void addToStorage() throws SQLException {
		setId(Storage.instance().add(new OrderTuple(null, getKind(), getStockName(), getAmount(), getPrice(), getDate(), asker.getBankAccountId())));
	}

	public void updateStorage() throws SQLException {
		Storage.instance().update(new OrderTuple(getId(), getKind(), getStockName(), getAmount(), getPrice(), getDate(), asker.getBankAccountId()));
		
	}

	@Override
	public void uploadInto(TradingManager exchange) {
		exchange.upload(this);
	}
}

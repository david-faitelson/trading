package exchange.trading.logic;

public class NoSuchAccountException extends Exception {

	public NoSuchAccountException(Integer bankAccountId) {
		this.bankAccountId = bankAccountId;
	}
	
	public Integer getBankAccountId() { return bankAccountId; }
	
	private Integer bankAccountId;
}

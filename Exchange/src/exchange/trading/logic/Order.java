package exchange.trading.logic;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;
import java.util.List;

public abstract class Order {
	
	public Order(Integer id, String name, Integer amnt, Integer prc, Date dt) {
		this.id = id;
		this.stockName = name;
		this.amount = amnt;
		this.price = prc;
		this.date = dt;
	}
		
	public String getStockName() { return stockName; }
	public Integer getAmount() { return amount; }
	public Integer getPrice() { return price; }
	public Date getDate() { return date; }

	public void decreaseAmountBy(Integer delta) {
		assert amount >= delta;
		amount = amount - delta;
	}

	public abstract Collection<? extends Transaction> matchIn(TradingManager exchange, List<Deposit> deposits) throws SQLException;
	
	public abstract String getKind(); 
	
	public abstract void addToStorage() throws SQLException;

	public abstract void uploadInto(TradingManager exchange);

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	private String stockName;
	private Integer amount;
	private Integer price;
	private Date date;
	private Integer id;

}

package exchange.trading.logic;

import java.util.Map;

import exchange.trading.storage.OrderTuple;

import java.util.HashMap;
import java.util.List;
import java.util.LinkedList;

public class Account {

	public Account(Integer bankAccId) {
		
		bankAccountId = bankAccId;
		
		orders = new HashMap<Integer, Order>();
		
	}

	public Integer getBankAccountId() { return bankAccountId; }

	public void placeAsk(Ask anAsk) {
		
		orders.put(anAsk.getId(), anAsk);
	}
	
	public void placeBid(Bid aBid) {
		
		orders.put(aBid.getId(), aBid);
	}
	
	public void clearEmptyOrders() {
		
		List<Integer> ordersToRemove = new LinkedList<Integer>();
		
		for(Order order : orders.values())
			if (order.getAmount() == 0)
				ordersToRemove.add(order.getId());
		
		for(Integer orderId : ordersToRemove)
			orders.remove(orderId);
	}

	public List<Integer> getOpenOrders() {
		
		List<Integer> orderIds = new LinkedList<Integer>();
		for(Order order : orders.values())
			if (order.getAmount()> 0)
				orderIds.add(order.getId());
		return orderIds;
	}
	
	public Order getOrder(Integer orderId) {
		return orders.get(orderId);
	}
	
	public Order upload(OrderTuple t) {
		
		assert t.getAccount() == bankAccountId;
		
		Order order = null;
		
		if (t.getKind().equals(OrderTuple.BID)) {
			order = new Bid(t.getId(), this, t.getStockName(), t.getAmount(), t.getPrice(), t.getDate());
		}
		else { 
			assert t.getKind().equals(OrderTuple.ASK);
			order = new Ask(t.getId(), this, t.getStockName(), t.getAmount(), t.getPrice(), t.getDate());		
		}
		
		orders.put(order.getId(), order);
		
		return order;
	}

	private Integer bankAccountId;
	private Map<Integer, Order> orders;
	
}

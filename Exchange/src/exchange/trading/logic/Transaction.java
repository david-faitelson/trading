package exchange.trading.logic;

public class Transaction {
	private Integer amountToTransfer;
	private Integer stockPrice;
	private String stockName;
	private Integer bidderBankAccountId;
	private Integer askerBankAccountId;

	public Transaction(Integer amountToTransfer, Integer stockPrice, String stockName, Integer bidderBankAccountId, Integer askerBankAccountId) {
		this.amountToTransfer = amountToTransfer;
		this.stockPrice = stockPrice;
		this.stockName = stockName;
		this.bidderBankAccountId = bidderBankAccountId;
		this.askerBankAccountId = askerBankAccountId;
	}

	public Integer getAmountToTransfer() {
		return amountToTransfer;
	}


	public Integer getStockPrice() {
		return stockPrice;
	}


	public String getStockName() {
		return stockName;
	}


	public Integer getBidderBankAccountId() {
		return bidderBankAccountId;
	}


	public Integer getAskerBankAccountId() {
		return askerBankAccountId;
	}
}

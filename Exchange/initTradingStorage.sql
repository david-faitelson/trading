connect 'jdbc:derby:EXCHANGE_TRADING_STORAGE;create=true';

create table OPEN_ORDERS 
	(id int not null generated always as identity (start with 1, increment by 1), 
	kind char, 
	stock varchar(20), 
	amount int, 
	price int, 
	creation_date timestamp, 
	account int);

create unique index open_orders_id on OPEN_ORDERS(id);
create index open_orders_account on OPEN_ORDERS(account);

create table STOCKS (name varchar(20));

insert into STOCKS values ('IBM');
insert into STOCKS values ('TEVA');
insert into STOCKS values ('INTEL');


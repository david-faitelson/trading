connect 'jdbc:derby:EXCHANGE_ASSETS_STORAGE;create=true';

create table ASSETS (account int, asset varchar(20), amount int);
create index assets_account on ASSETS(account);
create index assets_asset on ASSETS(asset);

create table LATEST_DEPOSIT (transaction_id int);

insert into LATEST_DEPOSIT values (0);

-- create an account for each group of students

insert into ASSETS values (101, 'NIS', 0);
insert into ASSETS values (102, 'NIS', 0);
insert into ASSETS values (103, 'NIS', 0);
insert into ASSETS values (104, 'NIS', 0);
insert into ASSETS values (105, 'NIS', 0);
insert into ASSETS values (106, 'NIS', 0);
insert into ASSETS values (107, 'NIS', 0);
insert into ASSETS values (108, 'NIS', 0);
insert into ASSETS values (109, 'NIS', 0);
insert into ASSETS values (110, 'NIS', 0);
insert into ASSETS values (201, 'NIS', 0);
insert into ASSETS values (202, 'NIS', 0);
insert into ASSETS values (203, 'NIS', 0);
insert into ASSETS values (204, 'NIS', 0);
insert into ASSETS values (205, 'NIS', 0);
insert into ASSETS values (206, 'NIS', 0);
insert into ASSETS values (207, 'NIS', 0);
insert into ASSETS values (208, 'NIS', 0);
insert into ASSETS values (209, 'NIS', 0);
insert into ASSETS values (210, 'NIS', 0);


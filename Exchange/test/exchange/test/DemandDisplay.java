package exchange.test;

import fit.RowFixture;

public class DemandDisplay extends RowFixture {

	@Override
	public Class getTargetClass() {
		return DemandSupplyTuple.class;
	}

	@Override
	public Object[] query() throws Exception {
		return ExchangeModel.instance().getDemand();
	}

}

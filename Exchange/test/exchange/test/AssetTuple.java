package exchange.test;

public class AssetTuple {

	public String name;
	public Integer amount;
	
	public AssetTuple(String name, Integer amount) {
		this.name = name;
		this.amount =amount;
	}
}

package exchange.test;

import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Set;

import auth.api.WrongSecretException;
import exchange.api.DoesNotHaveThisStockException;
import exchange.api.InternalExchangeErrorException;
import exchange.api.NoSuchAccountException;
import exchange.api.NotEnoughMoneyException;
import exchange.api.NotEnoughStockException;
import exchange.api.Order;
import exchange.api.StockNotTradedException;
import exchange.server.ExchangeServer;

public class ExchangeModel {
	
	private static ExchangeModel theExchange;

	public static ExchangeModel instance() throws IOException, NotBoundException, SQLException, exchange.api.NoSuchAccountException, InternalExchangeErrorException {
		if (theExchange == null)
			theExchange = new ExchangeModel();
		return theExchange;
	}

	private ExchangeServer exchange;
	private String secret;
	private Integer accountId;
	private String stockName;
	private Integer amount;
	private Integer price;
	
	public ExchangeModel() throws IOException, NotBoundException, SQLException, exchange.api.NoSuchAccountException, InternalExchangeErrorException {
		exchange = new ExchangeServer();
	}

	public void placeAsk() 
			throws RemoteException, WrongSecretException, NoSuchAccountException, NotEnoughStockException, StockNotTradedException, DoesNotHaveThisStockException, InternalExchangeErrorException 
	{

		exchange.placeAsk(secret, accountId, stockName, amount, price);
	}

	// Places a bid (request to buy) an amount of stockName at a price of (at most) price. 
	// Returns the identifier of the bid.
	
	public void placeBid() 
			throws RemoteException, WrongSecretException, NoSuchAccountException, NotEnoughStockException, StockNotTradedException, InternalExchangeErrorException, NotEnoughMoneyException {
	
		exchange.placeBid(secret, accountId, stockName, amount, price);
	}
	
	// Returns the list of identifiers of all the open orders of accountId. 
	// An order is open as long as its amount is not 0. 
	
	public Order[] getOpenOrders() throws RemoteException, NoSuchAccountException, WrongSecretException, InternalExchangeErrorException {
	
		List<Integer> orderIds = exchange.getOpenOrders(secret, accountId);
		
		Order[] openOrders = new Order[orderIds.size()];
		
		int i = 0;
		
		for (Integer orderId : orderIds) {
			openOrders[i]= exchange.getOrderDetails(secret, accountId, orderId);
			i++;
		}
		
		return openOrders;
	}
	
	// Returns the details of the order with identifier orderId, provided that it belongs to accountId. 
	

	public AssetTuple[] getAssets() throws RemoteException, NoSuchAccountException, WrongSecretException, InternalExchangeErrorException {
		
		Set<String> assetNames = exchange.getAssets(secret, accountId);
		AssetTuple[] assets = new AssetTuple[assetNames.size()];
		int i = 0;
		for(String assetName : assetNames) {
			assets[i] = new AssetTuple(assetName, exchange.getAmountOfAsset(secret, accountId, assetName));
			i++;		
		}
		return assets;
	}

	public Set<String> getStockNames() throws RemoteException {
		return exchange.getStockNames();
	}

	// Returns the supply of stockName. The keys are different prices, the values are the amounts of stock supplied at each price.
	
	public  DemandSupplyTuple[] getSupply() throws RemoteException {
		return convert(exchange.getSupply(stockName));
	}

	public  DemandSupplyTuple[] getDemand() throws RemoteException {
		return convert(exchange.getDemand(stockName));
	}

	private DemandSupplyTuple[] convert(Map<Integer, Integer> map) throws RemoteException {
		DemandSupplyTuple[] tuples = new DemandSupplyTuple[map.size()];
		int i = 0;
		for(Map.Entry<Integer, Integer> e : map.entrySet()) {
			tuples[i] = new DemandSupplyTuple(e.getKey(), e.getValue());
			i++;
		}
		return tuples;
	}
	
	// context
	
	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}

	public Integer getAccountId() {
		return accountId;
	}

	public void setAccountId(Integer accountId) {
		this.accountId = accountId;
	}

	public String getStockName() {
		return stockName;
	}

	public void setStockName(String stockName) {
		this.stockName = stockName;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

}

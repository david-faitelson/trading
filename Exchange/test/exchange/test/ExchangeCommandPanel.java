package exchange.test;

import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.sql.SQLException;

import auth.api.WrongSecretException;
import exchange.api.DoesNotHaveThisStockException;
import exchange.api.InternalExchangeErrorException;
import exchange.api.NotEnoughMoneyException;
import exchange.api.NotEnoughStockException;
import exchange.api.StockNotTradedException;
import exchange.api.NoSuchAccountException;
import fit.ActionFixture;

public class ExchangeCommandPanel extends ActionFixture {

	public void setAccountId(Integer accountId) throws IOException, NotBoundException, SQLException, NoSuchAccountException, InternalExchangeErrorException {
		ExchangeModel.instance().setAccountId(accountId);
	}

	public void setSecret(String secret) throws IOException, NotBoundException, SQLException, NoSuchAccountException, InternalExchangeErrorException {
		ExchangeModel.instance().setSecret(secret);
	}

	public void setStockName(String stockName) throws IOException, NotBoundException, SQLException, NoSuchAccountException, InternalExchangeErrorException {
		ExchangeModel.instance().setStockName(stockName);
	}

	public void setAmount(Integer amount) throws IOException, NotBoundException, SQLException, NoSuchAccountException, InternalExchangeErrorException {
		ExchangeModel.instance().setAmount(amount);
	}
	
	public void setPrice(Integer price) throws IOException, NotBoundException, SQLException, NoSuchAccountException, InternalExchangeErrorException {
		ExchangeModel.instance().setPrice(price);
	}
	
	public void placeBid() throws RemoteException, WrongSecretException, exchange.api.NoSuchAccountException, NotEnoughStockException, StockNotTradedException, InternalExchangeErrorException, NotEnoughMoneyException, IOException, NotBoundException, SQLException, NoSuchAccountException {
		ExchangeModel.instance().placeBid();
	}
	
	public void placeAsk() throws RemoteException, WrongSecretException, exchange.api.NoSuchAccountException, NotEnoughStockException, StockNotTradedException, DoesNotHaveThisStockException, InternalExchangeErrorException, IOException, NotBoundException, SQLException, NoSuchAccountException {
		ExchangeModel.instance().placeAsk();
	}
	
}

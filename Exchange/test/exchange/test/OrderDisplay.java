package exchange.test;

import exchange.api.Order;
import fit.RowFixture;

public class OrderDisplay extends RowFixture {

	@Override
	public Class getTargetClass() {
		return Order.class;
	}

	@Override
	public Object[] query() throws Exception {
		return ExchangeModel.instance().getOpenOrders();
	}

}

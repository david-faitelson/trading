package exchange.test;

public class DemandSupplyTuple {
	public Integer price;
	public Integer amount;
	public DemandSupplyTuple(Integer price, Integer amount) {
		this.price = price;
		this.amount = amount;
	}
}

package auth;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

public class TestNullInputs {

	@Test
	public void test() throws IOException {
		Authenticator auth = new Authenticator();
		
		assertEquals(auth.knowsPassword(null, null), false);
	}

}

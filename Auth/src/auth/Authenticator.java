package auth;

import java.io.IOException;
import java.util.Map;
import java.util.HashMap;
import java.util.Properties;

public class Authenticator {

    public Authenticator() throws IOException {
    
			Properties props = new Properties();
			props.load(Authenticator.class.getResourceAsStream("/passwords.properties"));

				passwords = new HashMap<Integer, String>();

			for(String key : props.stringPropertyNames()) {
				passwords.put(Integer.parseInt(key), props.getProperty(key));
			}
    }
    
    public boolean knowsPassword(Integer id, String secret) {
        String pswd = passwords.get(id);
        return pswd != null && pswd.equals(secret);
    }

    Map<Integer, String> passwords;
}


import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import java.util.Map;
import java.util.Scanner;

import auth.api.WrongSecretException;
import exchange.api.DoesNotHaveThisStockException;
import exchange.api.ExchangeManager;
import exchange.api.InternalExchangeErrorException;
import exchange.api.NoSuchAccountException;
import exchange.api.NotEnoughMoneyException;
import exchange.api.NotEnoughStockException;
import exchange.api.Order;
import exchange.api.StockNotTradedException;

public class ExchangeBrowser {

	public static void main(String[] args) throws Exception {

	if (args.length != 1) {
		System.out.println("Usage: java ExchangeBrowser <hostname>");
		System.exit(1);
	}

		new ExchangeBrowser(args[0]).run();
	}

	private ExchangeManager exchange;
	private Scanner scanner;
	
	public ExchangeBrowser(String hostName) throws MalformedURLException, RemoteException, NotBoundException {
		
		exchange = (ExchangeManager) Naming.lookup("rmi://"+hostName+"/Exchange");

		scanner = new Scanner(System.in);
	}
	
	public void run() throws RemoteException, NotEnoughMoneyException {
		
		System.out.println("Please select an operation:\n1 - show assets\n2 - show open orders \n3 - show supply & demand\n4 - place a command\n5 - exit");
		
		Integer opCode = scanner.nextInt();
		
		while(opCode != 5) {
			switch (opCode) { 
			case 1:
				showAssets(); break;
			case 2:
				showOpenOrders(); break;
			case 3:
				showSupplyAndDemand(); break;
			case 4:
				placeCommand(); break;
				
			}
			
			System.out.println("Please select an operation:\n1 - show assets\n2 - show open orders \n3 - show supply & demand\n4 - place a command\n5 - exit");
			opCode = scanner.nextInt();
		}
		
		scanner.close();
		System.out.println("Goodbye!");
	}

	private void showAssets() throws RemoteException {
		System.out.println("Please enter account identifier: ");
		Integer accountId = scanner.nextInt();
		System.out.println("Please enter account password: ");
		String secret = scanner.next();
		try {
			for(String assetName : exchange.getAssets(secret, accountId)) {
				System.out.println(assetName + " = " + exchange.getAmountOfAsset(secret, accountId, assetName));
			}
		} catch (NoSuchAccountException e) {
			System.out.println("There is no such account in the stock exchange.");
		} catch (WrongSecretException e) {
			System.out.println("The password is incorrect");
			e.printStackTrace();
		} catch (InternalExchangeErrorException e) {
			System.out.println("Internal server error: " + e.getMessage());
			e.printStackTrace();
		}
	}

	private void placeCommand() throws RemoteException, NotEnoughMoneyException {
		
		System.out.println("Please enter account identifier: ");
		Integer accountId = scanner.nextInt();
		System.out.println("Please enter account password: ");
		String secret = scanner.next();
		System.out.print("Please enter stock name: ");
		String stockName = scanner.next();
		System.out.print("Please enter amount of stock: ");
		Integer amount = scanner.nextInt();
		System.out.print("Please enter price: ");
		Integer price = scanner.nextInt();
		System.out.print("Select (B)id or (A)sk");
		String commandKind = scanner.next();
			
		try {
			if (commandKind.equals("B"))
				exchange.placeBid(secret, accountId, stockName, amount, price);
			else if (commandKind.equals("A"))
				exchange.placeAsk(secret, accountId, stockName, amount, price);
			else 
				System.out.println("Wrong option. You can select either A (for ask) or B (for bid).");
		} catch (WrongSecretException e) {
			System.out.println("The password is incorrect");
		} catch (NoSuchAccountException e) {
			System.out.println("There is no such account in the stock exchange.");
		} catch (NotEnoughStockException e) {
			System.out.println("There is not enough units of " + e.getAsset() + " in the account.");
		} catch (StockNotTradedException e) {
			System.out.println("This stock is not traded in the stock exchange.");
		} catch (DoesNotHaveThisStockException e) {
			System.out.println("The account does not hold this stock.");
		} catch (InternalExchangeErrorException e) {
			System.out.println("Internal server error: " + e.getMessage());
			e.printStackTrace();
		}
	}

	private void showSupplyAndDemand() throws RemoteException {
		
		System.out.println("Available stocks:");
		
		for(String stockName : exchange.getStockNames()) {
			System.out.println(stockName);
		}
		
		System.out.println("\nEnter name of stock: ");
		String stockName = scanner.next();
		
		System.out.println("Supply of stock " + stockName);
		System.out.println("Price\tAmount");
		Map<Integer, Integer> supply = exchange.getSupply(stockName);
		for(Map.Entry<Integer, Integer> quote : supply.entrySet()) {
			System.out.println(quote.getKey() + "\t" + quote.getValue());
		}

		System.out.println("Demand for stock " + stockName);
		System.out.println("Price\tAmount");
		Map<Integer, Integer> demand = exchange.getDemand(stockName);
		for(Map.Entry<Integer, Integer> quote : demand.entrySet()) {
			System.out.println(quote.getKey() + "\t" + quote.getValue());
		}
	}

	public void showOpenOrders() throws RemoteException {
		try {
			System.out.println("Please enter account identifier: ");
			Integer accountId = scanner.nextInt();
			System.out.println("Please enter account password: ");
			String password = scanner.next();
	
			for(Integer orderId : exchange.getOpenOrders(password, accountId)) {
				Order order = exchange.getOrderDetails(password, accountId, orderId);
				System.out.print(order.getCreationDate() + " ");
				if (order.getKind().equals(Order.ASK))
					System.out.print("Selling ");
				else
					System.out.print("Buying ");
				System.out.println(order.getAmount() + " " + order.getStockName() + " at a price of " + order.getPrice() + " NIS");
			}
		}
		catch(NoSuchAccountException e) {
			System.out.println("There is no such account in the stock exchange.");
		}
		catch(WrongSecretException e) {
			System.out.println("The password is incorrect");
		} catch (InternalExchangeErrorException e) {
			System.out.println("Internal server error: " + e.getMessage());
			e.printStackTrace();
		}
	}
}

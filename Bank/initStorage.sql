connect 'jdbc:derby:BANK_STORAGE;create=true';

create table ASSETS 
	(account int, 
	asset varchar(20), 
	amount int);
	
create index assets_account on ASSETS(account);

create index assets_asset on ASSETS(asset);

create table TRANSACTIONS 
	(id int not null generated always as identity (start with 1, increment by 1),
	kind char, 
	asset varchar(20), 
	amount int, 
	source int, 
	destination int, 
	execution_date timestamp);

create unique index transactions_id on TRANSACTIONS(id);

create index transactions_source on TRANSACTIONS(source);

create index transactions_date on TRANSACTIONS(execution_date);

-- Create an account for the stock exchange

insert into ASSETS values (3373, 'NIS', 0);

-- create an account for each group of students

insert into ASSETS values (101, 'NIS', 1000);
insert into ASSETS values (102, 'NIS', 1000);
insert into ASSETS values (103, 'NIS', 1000);
insert into ASSETS values (104, 'NIS', 1000);
insert into ASSETS values (105, 'NIS', 1000);
insert into ASSETS values (106, 'NIS', 1000);
insert into ASSETS values (107, 'NIS', 1000);
insert into ASSETS values (108, 'NIS', 1000);
insert into ASSETS values (109, 'NIS', 1000);
insert into ASSETS values (110, 'NIS', 1000);
insert into ASSETS values (201, 'NIS', 1000);
insert into ASSETS values (202, 'NIS', 1000);
insert into ASSETS values (203, 'NIS', 1000);
insert into ASSETS values (204, 'NIS', 1000);
insert into ASSETS values (205, 'NIS', 1000);
insert into ASSETS values (206, 'NIS', 1000);
insert into ASSETS values (207, 'NIS', 1000);
insert into ASSETS values (208, 'NIS', 1000);
insert into ASSETS values (209, 'NIS', 1000);
insert into ASSETS values (210, 'NIS', 1000);

-- add stocks to all the student groups

insert into ASSETS values (101, 'IBM', 30);
insert into ASSETS values (102, 'IBM', 30);
insert into ASSETS values (103, 'IBM', 30);
insert into ASSETS values (104, 'IBM', 30);
insert into ASSETS values (105, 'IBM', 30);
insert into ASSETS values (106, 'IBM', 30);
insert into ASSETS values (107, 'IBM', 30);
insert into ASSETS values (108, 'IBM', 30);
insert into ASSETS values (109, 'IBM', 30);
insert into ASSETS values (110, 'IBM', 30);
insert into ASSETS values (201, 'IBM', 30);
insert into ASSETS values (202, 'IBM', 30);
insert into ASSETS values (203, 'IBM', 30);
insert into ASSETS values (204, 'IBM', 30);
insert into ASSETS values (205, 'IBM', 30);
insert into ASSETS values (206, 'IBM', 30);
insert into ASSETS values (207, 'IBM', 30);
insert into ASSETS values (208, 'IBM', 30);
insert into ASSETS values (209, 'IBM', 30);
insert into ASSETS values (210, 'IBM', 30);

insert into ASSETS values (101, 'TEVA', 30);
insert into ASSETS values (102, 'TEVA', 30);
insert into ASSETS values (103, 'TEVA', 30);
insert into ASSETS values (104, 'TEVA', 30);
insert into ASSETS values (105, 'TEVA', 30);
insert into ASSETS values (106, 'TEVA', 30);
insert into ASSETS values (107, 'TEVA', 30);
insert into ASSETS values (108, 'TEVA', 30);
insert into ASSETS values (109, 'TEVA', 30);
insert into ASSETS values (110, 'TEVA', 30);
insert into ASSETS values (201, 'TEVA', 30);
insert into ASSETS values (202, 'TEVA', 30);
insert into ASSETS values (203, 'TEVA', 30);
insert into ASSETS values (204, 'TEVA', 30);
insert into ASSETS values (205, 'TEVA', 30);
insert into ASSETS values (206, 'TEVA', 30);
insert into ASSETS values (207, 'TEVA', 30);
insert into ASSETS values (208, 'TEVA', 30);
insert into ASSETS values (209, 'TEVA', 30);
insert into ASSETS values (210, 'TEVA', 30);

insert into ASSETS values (101, 'INTEL', 30);
insert into ASSETS values (102, 'INTEL', 30);
insert into ASSETS values (103, 'INTEL', 30);
insert into ASSETS values (104, 'INTEL', 30);
insert into ASSETS values (105, 'INTEL', 30);
insert into ASSETS values (106, 'INTEL', 30);
insert into ASSETS values (107, 'INTEL', 30);
insert into ASSETS values (108, 'INTEL', 30);
insert into ASSETS values (109, 'INTEL', 30);
insert into ASSETS values (110, 'INTEL', 30);
insert into ASSETS values (201, 'INTEL', 30);
insert into ASSETS values (202, 'INTEL', 30);
insert into ASSETS values (203, 'INTEL', 30);
insert into ASSETS values (204, 'INTEL', 30);
insert into ASSETS values (205, 'INTEL', 30);
insert into ASSETS values (206, 'INTEL', 30);
insert into ASSETS values (207, 'INTEL', 30);
insert into ASSETS values (208, 'INTEL', 30);
insert into ASSETS values (209, 'INTEL', 30);
insert into ASSETS values (210, 'INTEL', 30);

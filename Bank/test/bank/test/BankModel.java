package bank.test;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Date;
import java.util.List;
import java.util.Set;

import auth.api.WrongSecretException;
import bank.server.BankServer;
import bank.api.DoesNotHaveThisAssetException;
import bank.api.InternalServerErrorException;
import bank.api.NotEnoughAssetException;
import bank.api.Transaction;

public class BankModel {
	
	private static BankModel theBank;

	public static BankModel instance() throws IOException {
		if (theBank == null)
			theBank = new BankModel();
		return theBank;
	}

	private BankServer bank;
	
	public BankModel() throws IOException {
		bank = new BankServer();
	}
	
	// context
	
	private Integer sourceAccount;
	private String sourceSecret;
	private Integer destinationAccount;
	private String assetToTransfer;
	private Integer amountToTransfer;
	
	public Integer getAmountToTransfer() {
		return amountToTransfer;
	}

	public void setAmountToTransfer(Integer amountToTransfer) {
		this.amountToTransfer = amountToTransfer;
	}

	private Date dateOfEarliestTransaction;
	
	
	// bank operations
	
	public void transfer() throws RemoteException, DoesNotHaveThisAssetException, NotEnoughAssetException, WrongSecretException, InternalServerErrorException {
		assert sourceAccount != null;
		assert destinationAccount != null;
		assert assetToTransfer != null;
		assert amountToTransfer != null;
		
		bank.transferAssets(sourceSecret, sourceAccount, destinationAccount, assetToTransfer, amountToTransfer);
	}
	
	public AssetTuple[] getAssets() throws RemoteException, WrongSecretException, InternalServerErrorException, DoesNotHaveThisAssetException {
		assert sourceAccount != null;
		assert destinationAccount != null;
		
		Set<String> assets = bank.getAssets(sourceSecret, sourceAccount);
	
		AssetTuple[] tuples = new AssetTuple[assets.size()];
		
		int i = 0;
		
		for(String asset : assets) {
			tuples[i] = new AssetTuple(asset, bank.getQuantityOfAsset(sourceSecret,sourceAccount, asset));	
			i++;
		}
		
		return tuples;
	}

	public Object[] getTransactions() throws RemoteException, WrongSecretException, InternalServerErrorException {
		List<Transaction> txns = bank.getTransactionsSince(sourceSecret, sourceAccount, dateOfEarliestTransaction);
		return txns.toArray();
	}

	public void clearCache() {
		bank.clearCache();
	}
	
	// context

	public String getAssetToTransfer() {
		return assetToTransfer;
	}

	public void setAssetToTransfer(String assetToTransfer) {
		this.assetToTransfer = assetToTransfer;
	}

	public Integer getSourceAccount() {
		return sourceAccount;
	}

	public void setSourceAccount(Integer sourceAccount) {
		this.sourceAccount = sourceAccount;
	}

	public String getSourceSecret() {
		return sourceSecret;
	}

	public void setSourceSecret(String sourceSecret) {
		this.sourceSecret = sourceSecret;
	}

	public Integer getDestinationAccount() {
		return destinationAccount;
	}

	public void setDestinationAccount(Integer destinationAccount) {
		this.destinationAccount = destinationAccount;
	}

	public Date getDateOfEarliestTransaction() {
		return dateOfEarliestTransaction;
	}

	public void setDateOfEarliestTransaction(Date dateOfEarliestTransaction) {
		this.dateOfEarliestTransaction = dateOfEarliestTransaction;
	}
}

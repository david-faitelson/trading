package bank.test;

import java.io.IOException;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import auth.api.WrongSecretException;
import bank.api.DoesNotHaveThisAssetException;
import bank.api.InternalServerErrorException;
import bank.api.NotEnoughAssetException;
import fit.ActionFixture;

public class BankCommandPanel extends ActionFixture {
	
	public void setSourceAccount(Integer accountId) throws IOException {
		BankModel.instance().setSourceAccount(accountId);
	}
	
	public void setSourcePassword(String password) throws IOException {
		BankModel.instance().setSourceSecret(password);
	}
	
	public void setAssetToTransfer(String assetName) throws IOException {
		BankModel.instance().setAssetToTransfer(assetName);
	}

	public void setAmountToTransfer(Integer amount) throws IOException {
		BankModel.instance().setAmountToTransfer(amount);
	}

	public void setDestinationAccount(Integer accountId) throws IOException {
		BankModel.instance().setDestinationAccount(accountId);
	}
	
	public void setDateOfEarliestTransaction(String dateText) throws IOException, ParseException {
		BankModel.instance().setDateOfEarliestTransaction(new SimpleDateFormat("dd/MM/yy").parse(dateText));
	}
	
	public void transfer() throws RemoteException, DoesNotHaveThisAssetException, NotEnoughAssetException, WrongSecretException, InternalServerErrorException, IOException {
		BankModel.instance().transfer();
	}
	
	public void clearCache() throws IOException {
		BankModel.instance().clearCache();
	}
}

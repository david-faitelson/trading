package bank.test;

import fit.RowFixture;

public class AssetDisplay extends RowFixture {

	@Override
	public Class getTargetClass() {
		return AssetTuple.class;
	}

	@Override
	public Object[] query() throws Exception {
		return BankModel.instance().getAssets();
	}

}

package bank.test;

import fit.RowFixture;

public class TransactionDisplay extends RowFixture {

	@Override
	public Class getTargetClass() {
		return bank.api.Transaction.class;
	}

	@Override
	public Object[] query() throws Exception {
		Object[] txns = BankModel.instance().getTransactions();
		return txns;
	}

}

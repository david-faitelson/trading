package bank.server;

import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import java.io.IOException;

//import java.sql.SQLException;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.sql.SQLException;

import bank.api.BankManager;
import bank.api.DoesNotHaveThisAssetException;
import bank.api.InternalServerErrorException;
import bank.api.NotEnoughAssetException;
import bank.api.Transaction;
import bank.logic.Account;
import bank.logic.Bank;

import auth.api.WrongSecretException;
import auth.Authenticator;

public class BankServer implements BankManager {

    public static void main(String[] args) {
        try {
        	
        	if (args.length != 1) {
        		System.out.println("Usage: java bank.server.BankServer <port>");
        		System.exit(1);
        	}
        	
        	Integer port = Integer.parseInt(args[0]);
        	
            String name = "Bank";
            BankServer server = new BankServer();
            BankManager stub =
                (BankManager) UnicastRemoteObject.exportObject(server, port);
            Registry registry = LocateRegistry.getRegistry();
            registry.rebind(name, stub);
            System.out.println("The bank is open.");
        } 
        catch (Exception e) {
            System.err.println("Bank server exception:");
            e.printStackTrace();
        }
    }
    
    public BankServer() throws IOException {
    	authenticator = new Authenticator();
    	bank = new Bank();
    }

    public synchronized void transferAssets(String secret, Integer sourceAccount, 
    		Integer destinationAccount, String asset, Integer amount) 
    				throws DoesNotHaveThisAssetException, NotEnoughAssetException, RemoteException, WrongSecretException, InternalServerErrorException {
    		if (authenticator.knowsPassword(sourceAccount, secret)) {
    			try {
    				
    				Account source, dest;
    				
    				source = bank.getAccount(sourceAccount);
    				
    				if (source == null)
    					throw new WrongSecretException(secret, sourceAccount);
    				
    				dest = bank.getAccount(destinationAccount);
    				
    				if (dest == null)	
    					throw new WrongSecretException(secret, destinationAccount);
    				
    				bank.transferAssets(source, dest, asset, amount);
    			}
    			catch(bank.logic.DoesNotHaveThisAssetException err) {
    				throw new DoesNotHaveThisAssetException(err.getAccount().getId(), asset);
    			}
    			catch(bank.logic.NotEnoughAssetException err) {
    				throw new NotEnoughAssetException(err.getAccount().getId(), err.getAsset(), err.getCurrentAmount(), err.getRequestedAmount());
    			} catch (SQLException e) {
					throw new InternalServerErrorException(e.getMessage());
				}
    		}
    		else
    			throw new WrongSecretException("transferAssets", sourceAccount);
	}

    public synchronized List<Transaction> getTransactionsSince(String secret, Integer account, Date date) 
    		throws RemoteException, WrongSecretException, InternalServerErrorException {
    	
    	if (date == null)
    		throw new IllegalArgumentException("date");
    	
		if (authenticator.knowsPassword(account, secret)) {
			List<Transaction> txns = new LinkedList<Transaction>();
			try {
				Account acc = bank.getAccount(account);
				
				if (acc == null)
					throw new WrongSecretException(secret, account);
				
				List<bank.logic.Transaction> transactions = acc.getTransactionsSince(date, bank);
				
				assert transactions != null;
				
				for(bank.logic.Transaction t : transactions) {
					txns.add(new Transaction(t.getId(), t.getKind(), t.getAmount(), t.getAsset(), t.getSource().getId(), t.getDestination().getId(), t.getExecutionDate()));
				}
				return txns;
			} catch (SQLException e) {
				throw new InternalServerErrorException(e.getMessage());
			}
		}
		else
			throw new WrongSecretException("getTransactionsSince", account);
	}

    public synchronized Set<String> getAssets(String secret, Integer account) 
		throws RemoteException, WrongSecretException, InternalServerErrorException {
		
    	if (authenticator.knowsPassword(account, secret)) {
    		try {
				return new HashSet<String>(bank.getAccount(account).getAssets());
			} catch (SQLException e) {
				throw new InternalServerErrorException(e.getMessage());
			}
    	}
    	else
			throw new WrongSecretException("getAssets", account);    		
	}

    public synchronized Integer getQuantityOfAsset(String secret, Integer account, String asset) 
		throws RemoteException, WrongSecretException, DoesNotHaveThisAssetException, InternalServerErrorException {
	
    	if (authenticator.knowsPassword(account, secret)) {
    		try {
    			return bank.getAccount(account).getQuantityOfAsset(asset);
    		}
    		catch(bank.logic.DoesNotHaveThisAssetException err) {
    			throw new DoesNotHaveThisAssetException(err.getAccount().getId(), err.getAsset());
    		} catch (SQLException e) {
				throw new InternalServerErrorException(e.getMessage());
			}
    	}
    	else
    		throw new WrongSecretException("getQuantityOfAsset", account);
	}

    @Override
	public synchronized List<Transaction> getDepositsAfter(String secret, Integer account, Integer transactionId) throws InternalServerErrorException, WrongSecretException {
    	if (authenticator.knowsPassword(account, secret)) {
    		try {
    			List<Transaction> txns = new LinkedList<Transaction>();
    			
    			for(bank.logic.Transaction t : bank.getAccount(account).getDepositsAfter(transactionId, bank)) {
					txns.add(new Transaction(t.getId(), t.getKind(), t.getAmount(), t.getAsset(), t.getSource().getId(), t.getDestination().getId(), t.getExecutionDate()));
				}
				return txns;
    		} catch (SQLException e) {
				throw new InternalServerErrorException(e.getMessage());
			}
    	}
    	else
    		throw new WrongSecretException("getDepositsAfter", account);

	}
    
    public synchronized void clearCache() {
    	bank.clearCache();
    }

    private Authenticator authenticator;
    private Bank bank;


}

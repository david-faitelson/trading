package bank.logic;

import java.util.Date;

public class Deposit extends Transaction {
	public Deposit(Integer id, Integer amount, String asset, Account source, Account destination, Date executionDate) {
        super(id, amount, asset, source, destination, executionDate);
    }

	public String getKind() { return "D"; }

}

package bank.logic;

import java.sql.SQLException;
import java.util.Date;
import java.util.Map;

import bank.storage.AssetTuple;
import bank.storage.Storage;

import java.util.HashMap;
import java.util.List;

public class Bank {
	
	private Map<Integer, Account> accounts;

	public Bank() {

		accounts = new HashMap<Integer, Account>();
	}
	
	public Account getAccount(Integer accountIdentifier) throws SQLException {
		
		Account account = accounts.get(accountIdentifier);
		
		if (account == null) { // perhaps it was not yet loaded from storage
			
			List<AssetTuple> assets = Storage.instance().loadAccountAssets(accountIdentifier);
			
			if (!assets.isEmpty()) { // yes, there is such an account. create the object and fill it with assets
				account = new Account(accountIdentifier, assets);
				accounts.put(accountIdentifier, account);
			}
		}

		return account;
	}
	
	public void transferAssets(Account source, Account destination, String asset, Integer amount) 
			throws NotEnoughAssetException, DoesNotHaveThisAssetException, SQLException {
		try {
			Date now = new Date();
			Integer withdrawId = source.giveAssetsFromStorage(amount, asset, destination, now);
			Integer depositId = destination.takeAssetsFromStorage(amount, asset, source, now);
			Storage.instance().commit();
			
			// at this point we know that the changes have made it to the storage and 
			// that the conditions necessary for the cache operations to succeed have been met
			
			source.giveAssetsFromCache(withdrawId, amount, asset, destination, now);
			destination.takeAssetsFromCache(depositId, amount, asset, source, now);
		}
		catch(Exception e) {
			Storage.instance().abort();
			throw e;
		}
	}
	
	public void clearCache() {
		accounts.clear();
	}
}

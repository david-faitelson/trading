package bank.logic;

import java.util.Date; 
import java.util.Set;

import bank.storage.AssetTuple;
import bank.storage.Storage;
import bank.storage.TransactionTuple;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

public class Account {

	private Integer identifier;
	private Map<String, Integer> assets;
	private List<Transaction> transactions;
	private Date earliestTransaction;

	public Account(Integer identifier, List<AssetTuple> assetTuples) {

		this.earliestTransaction = new Date();
		
		this.identifier = identifier;
		this.assets = new HashMap<String, Integer>();
		this.transactions = new LinkedList<Transaction>();
		
		for(AssetTuple tuple : assetTuples) {
			assets.put(tuple.getAsset(), tuple.getAmount());
		}
	}

	public Integer getId() {
		return identifier;
	}
	
	public Integer giveAssetsFromStorage(Integer amount, String asset, Account destination, Date executionDate) throws DoesNotHaveThisAssetException, NotEnoughAssetException, SQLException {

		Integer currentAmount = getQuantityOfAsset(asset);

		if (currentAmount < amount)
			throw new NotEnoughAssetException(this, asset, currentAmount, amount);

		Storage.instance().update(new AssetTuple(identifier, asset, currentAmount - amount));
		
		Withdraw withdraw = new Withdraw(null, amount, asset, this, destination, executionDate);
		
		withdraw.store();
		
		return withdraw.getId();
	}
	
	public void giveAssetsFromCache(Integer id, Integer amount, String asset, Account destination, Date executionDate) throws DoesNotHaveThisAssetException, NotEnoughAssetException {

		Integer currentAmount = getQuantityOfAsset(asset);

		if (currentAmount < amount)
			throw new NotEnoughAssetException(this, asset, currentAmount, amount);

		assets.put(asset, currentAmount - amount);
		transactions.add(0, new Withdraw(id, amount, asset, this, destination, executionDate));
	}

	public Integer takeAssetsFromStorage(Integer amount, String asset, Account source, Date executionDate) throws SQLException {

		Integer currentAmount = assets.get(asset);

		if (currentAmount == null) {
			Storage.instance().add(new AssetTuple(identifier, asset, amount));
		}
		else {
			Storage.instance().update(new AssetTuple(identifier, asset, currentAmount + amount));
		}

		Deposit deposit = new Deposit(null, amount, asset, source, this, executionDate);
		
		deposit.store();
		
		return deposit.getId();
	}
	
	public void takeAssetsFromCache(Integer id, Integer amount, String asset, Account source, Date executionDate) {

		Integer currentAmount = assets.get(asset);

		if (currentAmount == null) {
			assets.put(asset, amount);
		}
		else {
			assets.put(asset, currentAmount + amount);
		}
		
		transactions.add(0, new Deposit(id, amount, asset, source, this, executionDate));
	}

	public Set<String> getAssets() {
		return assets.keySet();
	}

	public Integer getQuantityOfAsset(String asset) throws DoesNotHaveThisAssetException {
		Integer currentAmount = assets.get(asset);
		if (currentAmount == null)
			throw new DoesNotHaveThisAssetException(this, asset);
		return currentAmount;
	}

	public List<Transaction> getTransactionsSince(Date date, Bank bank) throws SQLException {
		
		assert date != null;
		
		if (date.before(earliestTransaction))
			reloadTransactions(date, bank);
		
		List<Transaction> result = new ArrayList<Transaction>();
		Iterator<Transaction> iter = transactions.iterator();
		Transaction t;
		while(iter.hasNext() && (t = iter.next()).getExecutionDate().after(date)) {
			result.add(t);
		}
		return result;
	}

	private void reloadTransactions(Date date, Bank bank) throws SQLException {
		
		transactions.clear();
		for(TransactionTuple tuple : Storage.instance().loadTransactionsSince(identifier, date)) {
			if (tuple.getKind().equals("D")) 
				transactions.add(new Deposit(tuple.getId(), tuple.getAmount(),tuple.getAsset(), bank.getAccount(tuple.getSource()), bank.getAccount(tuple.getDestination()), tuple.getExecutionDate()));
			else 
				transactions.add(new Withdraw(tuple.getId(), tuple.getAmount(),tuple.getAsset(), bank.getAccount(tuple.getSource()), bank.getAccount(tuple.getDestination()), tuple.getExecutionDate()));
		}
		
		earliestTransaction = date;
	}

	public List<Transaction> getDepositsAfter(Integer transactionId, Bank bank) throws SQLException {
		
		List<Transaction> deposits = new LinkedList<Transaction>();
		
		for(TransactionTuple tuple : Storage.instance().getDepositsAfter(identifier, transactionId)) {
			deposits.add(new Deposit(tuple.getId(), tuple.getAmount(), tuple.getAsset(), bank.getAccount(tuple.getSource()), bank.getAccount(tuple.getDestination()), tuple.getExecutionDate()));
		}
		
		return deposits;
	}
}

package bank.logic;

import java.sql.SQLException;
import java.util.Date;

import bank.storage.Storage;
import bank.storage.TransactionTuple;

public abstract class Transaction {

	private Integer id;
	private Integer amount;
	private String asset;
	private Account source;
	private Account destination;
	private Date executionDate;

	public Transaction(Integer id, Integer amount, String asset, Account source, Account destination, Date executionDate) {
		this.id = id;
		this.amount = amount;
		this.asset = asset;
		this.source = source;
		this.destination = destination;
		this.executionDate = executionDate;
	}

	public Integer getAmount() {
		return amount;
	}

	public String getAsset() {
		return asset;
	}

	public Account getSource() {
		return source;
	}

	public Account getDestination() {
		return destination;
	}

	public Date getExecutionDate() {
		return executionDate;
	}
	
	public abstract String getKind();
	
	public void store() throws SQLException { /* kind char, asset varchar(20), amount int, source int, destination int*/ 
		id = Storage.instance().add(new TransactionTuple(null, getKind(), asset, amount, source.getId(), destination.getId(), new Date()));
	}

	public Integer getId() {
		return id;
	}

}

package bank.logic;

import java.util.Date;

public class Withdraw extends Transaction {

	public Withdraw(Integer id, Integer amount, String asset, Account source, Account destination, Date executionDate) {
		super(id, amount, asset, source, destination, executionDate);
	}

	public String getKind() { return "W"; }
}

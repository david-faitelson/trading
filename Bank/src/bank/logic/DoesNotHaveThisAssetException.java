package bank.logic;

public class DoesNotHaveThisAssetException extends Exception {
	private String asset;
	private Account account;

	public DoesNotHaveThisAssetException(Account account, String asset) {
		this.account = account;
		this.asset = asset;
	}

	public String getAsset() {
		return asset;
	}

	public Account getAccount() {
		return account;
	}
}

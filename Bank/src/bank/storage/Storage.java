package bank.storage;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class Storage {

	private static Storage storage;

	public static Storage instance() throws SQLException {
		if (storage == null)
			storage = new Storage();
		return storage;
	}

	private Connection conn;
	private PreparedStatement updateAssetStmt;
	private PreparedStatement insertAssetStmt;
	private PreparedStatement insertTransactionStmt;
	private PreparedStatement selectAssetsStmt;
	private PreparedStatement selectTransactionsStmt;
	private PreparedStatement selectDepositsStmt;

	public Storage() throws SQLException {
		conn = DriverManager.getConnection("jdbc:derby:"+"BANK_STORAGE");   
        conn.setAutoCommit(false);
        
        updateAssetStmt = conn.prepareStatement("update ASSETS set amount = ? where account = ? and asset = ?");
        insertAssetStmt = conn.prepareStatement("insert into ASSETS values (?,?,?)");       
        selectAssetsStmt = conn.prepareStatement("select * from ASSETS where account = ?");
        insertTransactionStmt = conn.prepareStatement("insert into TRANSACTIONS (kind, asset, amount, source, destination, execution_date) values (?,?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
        selectTransactionsStmt = conn.prepareStatement("select * from TRANSACTIONS where ((kind = 'W' and source = ?) or (kind = 'D' and destination = ?)) and execution_date >= ?");
        selectDepositsStmt = conn.prepareStatement("select * from TRANSACTIONS where kind = 'D' and destination = ? and id > ?");
	}
	
	public void add(AssetTuple assetTuple) throws SQLException {
		insertAssetStmt.setInt(1, assetTuple.getAccount());
		insertAssetStmt.setString(2, assetTuple.getAsset());
		insertAssetStmt.setInt(3, assetTuple.getAmount());
		insertAssetStmt.executeUpdate();
	}
	
	public void update(AssetTuple assetTuple) throws SQLException {

		updateAssetStmt.setInt(1, assetTuple.getAmount());
		updateAssetStmt.setInt(2, assetTuple.getAccount());
		updateAssetStmt.setString(3, assetTuple.getAsset());
		updateAssetStmt.executeUpdate();
	}

	public Integer add(TransactionTuple t) throws SQLException {
		/* create table TRANSACTIONS (kind char, asset varchar(20), amount int, source int, destination int, execution_date timestamp);
 */
		insertTransactionStmt.setString(1, t.getKind());
		insertTransactionStmt.setString(2, t.getAsset());
		insertTransactionStmt.setInt(3,t.getAmount());
		insertTransactionStmt.setInt(4,t.getSource());
		insertTransactionStmt.setInt(5,t.getDestination());
		insertTransactionStmt.setTimestamp(6, new Timestamp(t.getExecutionDate().getTime()));
		
		insertTransactionStmt.executeUpdate();
		
		ResultSet r = null;
		
		try {
			r = insertTransactionStmt.getGeneratedKeys();
			r.next(); 
			return r.getInt(1);
		} finally {
			if (r != null)
				r.close();
		}
	}

	public List<AssetTuple> loadAccountAssets(Integer accountId) throws SQLException {
		
		selectAssetsStmt.setInt(1, accountId);
        
        ResultSet r = null;
        try {
            r = selectAssetsStmt.executeQuery();
        
            List<AssetTuple> rs = new LinkedList<AssetTuple>();
        
            while (r.next())
                rs.add(makeTuple(r));
            
            return rs;
            
        } finally {
            if (r != null)
                r.close();
        }
	}
	
	private AssetTuple makeTuple(ResultSet r) throws SQLException {
	      return new AssetTuple(r.getInt(1), r.getString(2), r.getInt(3)); 
	}

	public List<TransactionTuple> loadTransactionsSince(Integer accountId, Date date) throws SQLException {

		selectTransactionsStmt.setInt(1, accountId);
		selectTransactionsStmt.setInt(2, accountId);
		selectTransactionsStmt.setTimestamp(3, new Timestamp(date.getTime()));
        
        ResultSet r = null;
        try {
            r = selectTransactionsStmt.executeQuery();
        
            List<TransactionTuple> rs = new LinkedList<TransactionTuple>();
        
            while (r.next())
                rs.add(makeTransactionTuple(r));
            
            return rs;
            
        } finally {
            if (r != null)
                r.close();
        }
	}
	
	private TransactionTuple makeTransactionTuple(ResultSet r) throws SQLException {
		return new TransactionTuple(r.getInt("id"), r.getString("kind"), r.getString("asset"), r.getInt("amount"), r.getInt("source"), r.getInt("destination"), r.getTimestamp("execution_date"));
	}

	public void commit() throws SQLException {
		conn.commit();		
	}
	
	public void abort() throws SQLException {
		conn.rollback();
	}

	public List<TransactionTuple> getDepositsAfter(Integer accountId, Integer transactionId) throws SQLException {
		selectDepositsStmt.setInt(1, accountId);
		selectDepositsStmt.setInt(2, transactionId);

		ResultSet r = null;
        try {
            r = selectDepositsStmt.executeQuery();
        
            List<TransactionTuple> rs = new LinkedList<TransactionTuple>();
        
            while (r.next())
                rs.add(makeTransactionTuple(r));
            
            return rs;
            
        } finally {
            if (r != null)
                r.close();
        }
	}

}

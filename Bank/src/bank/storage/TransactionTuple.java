package bank.storage;

import java.util.Date;

public class TransactionTuple {

	private Integer id;
	private String kind;
	private String asset;
	private Integer amount;
	private Integer source;
	private Integer destination;
	private Date executionDate;

	public TransactionTuple(Integer id, String kind, String asset, Integer amount, Integer source, Integer destination, Date executionDate) {
		this.id = id;
		this.kind = kind;
		this.asset = asset;
		this.amount = amount;
		this.source = source;
		this.destination = destination;
		this.executionDate = executionDate;
	}

	public String getKind() {
		return kind;
	}

	public void setKind(String kind) {
		this.kind = kind;
	}

	public String getAsset() {
		return asset;
	}

	public void setAsset(String asset) {
		this.asset = asset;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public Integer getSource() {
		return source;
	}

	public void setSource(Integer source) {
		this.source = source;
	}

	public Integer getDestination() {
		return destination;
	}

	public void setDestination(Integer destination) {
		this.destination = destination;
	}

	public Date getExecutionDate() {
		return executionDate;
	}

	public void setExecutionDate(Date executionDate) {
		this.executionDate = executionDate;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
}
